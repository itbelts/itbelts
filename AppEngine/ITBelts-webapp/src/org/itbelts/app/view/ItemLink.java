package org.itbelts.app.view;

import org.itbelts.domain.AbstractType;

/**
 * Class that describes a link on the topic page (content.jsp)
 * The link points towards a course, a workshop, or an exam.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 27-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      27-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ItemLink extends AbstractType {

    private static final long serialVersionUID = -7743937150958407488L;

    private String linkType;
    private String url;
    private String name;

    
    /**
     * @return Returns the linkType.
     */
    public String getLinkType() {
        return linkType;
    }
    /**
     * @param aLinkType The linkType to set.
     */
    public void setLinkType( String aLinkType ) {
        linkType = aLinkType;
    }
    /**
     * @return Returns the url.
     */
    public String getUrl() {
        return url;
    }
    /**
     * @param aUrl The url to set.
     */
    public void setUrl( String aUrl ) {
        url = aUrl;
    }
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }
    
}
