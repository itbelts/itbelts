package org.itbelts.app.controller;

import org.itbelts.app.controller.business.CourseAccess;
import org.itbelts.app.controller.business.UserLoggedOn;
import org.itbelts.dao.ICourseDAO;
import org.itbelts.domain.Course;
import org.itbelts.domain.User;
import org.itbelts.exception.UnknownResourceException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@Controller
public class CourseContentController {
 
    private ICourseDAO          courseDAO;
    private UserLoggedOn        securityChecker;
    private CourseAccess        courseAccessChecker;

    @RequestMapping( value = "/course-content.htm", method = RequestMethod.POST )
    public ModelAndView showCourseDetails( @RequestParam( value="id" ) String aCourseId ) {

        // Will throw exception when the user is not logged on
        try {
            User theUser = securityChecker.getLoggedOnUser();
            return forwardToContent( theUser, aCourseId );
        
        } catch ( UnknownResourceException e ) {
            UserService userService = UserServiceFactory.getUserService();
            ModelAndView model = new ModelAndView("course-content");
            model.setViewName( "redirect:" + userService.createLoginURL("/course-content.htm?id=" + aCourseId ) );
            return model;
        }
    }

    @RequestMapping( value = "/course-content.htm", method = RequestMethod.GET )
    public ModelAndView showCourseDetailsForLoggedOnUser( @RequestParam( value="id" ) String aCourseId ) {
        
        // Will throw exception when the user is not logged on
        User theUser = securityChecker.getLoggedOnUser();

        return forwardToContent( theUser, aCourseId );
    }
    
    private ModelAndView forwardToContent( User aUser, String aCourseId ) {
        Course theCourse = courseDAO.getCourse( aCourseId );
        
        ModelAndView model = new ModelAndView();
        model.addObject( "course", theCourse );
        if ( courseAccessChecker.hasAccessToCourse( aUser, theCourse ) ) {
            model.setViewName( "course-content" );
        } else {
            model.setViewName( "hire-coach" );
        }
        return model;
    }
    
    /**
     * @param aCourseDAO The courseDAO to set.
     */
    public void setCourseDAO( ICourseDAO aCourseDAO ) {
        courseDAO = aCourseDAO;
    }
    /**
     * @param aSecurityChecker The securityChecker to set.
     */
    public void setSecurityChecker( UserLoggedOn aSecurityChecker ) {
        securityChecker = aSecurityChecker;
    }

    /**
     * @param aCourseAccessChecker The courseAccessChecker to set.
     */
    public void setCourseAccessChecker( CourseAccess aCourseAccessChecker ) {
        courseAccessChecker = aCourseAccessChecker;
    }
    
}
