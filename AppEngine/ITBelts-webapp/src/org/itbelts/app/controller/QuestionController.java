package org.itbelts.app.controller;

import org.itbelts.dao.IQuestionDAO;
import org.itbelts.domain.Question;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class QuestionController {
 
    private IQuestionDAO questionDAO;
    
    @RequestMapping( value = "/question.htm", method = RequestMethod.GET )
    public ModelAndView showQuestionDetails( @RequestParam( value="id" ) String aQuestionId ) {

        Question theQuestion = questionDAO.getQuestion( aQuestionId );

        ModelAndView model = new ModelAndView("question");
        model.addObject( "question", theQuestion );
        
        return model;
    }

    /**
     * @param aQuestionDAO The questionDAO to set.
     */
    public void setQuestionDAO( IQuestionDAO aQuestionDAO ) {
        questionDAO = aQuestionDAO;
    }

}
