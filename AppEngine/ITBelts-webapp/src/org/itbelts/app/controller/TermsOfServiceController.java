package org.itbelts.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TermsOfServiceController {
 
    @RequestMapping( value = "/terms-of-service.htm", method = RequestMethod.GET )
    public String support() {
        return "terms-of-service";
    }
}
