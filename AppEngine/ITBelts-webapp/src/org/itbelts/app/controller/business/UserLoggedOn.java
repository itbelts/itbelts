package org.itbelts.app.controller.business;

import org.itbelts.dao.IUserDAO;
import org.itbelts.domain.User;
import org.itbelts.exception.UnknownResourceException;

import com.google.appengine.api.users.UserServiceFactory;

/**
 *  Class that has business logic to check wether a user is properly logged on and all relevant information is available.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 27-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      27-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class UserLoggedOn {

    private IUserDAO            userDAO;

    /**
     * Check if this user is logged on.<br>
     * Access is granted to:<br>
     * <ul>
     *    <li>a user which has a logged on Google account
     *    <li>a user for which there is ITBelts user information in the MongoDB
     * </ul>
     * @return {@link User}
     */
    public User getLoggedOnUser() {
        com.google.appengine.api.users.User googleUser = UserServiceFactory.getUserService().getCurrentUser();
        if ( googleUser == null ) {
            // Should not happen. Only occurs when the user manipulates the URL without having logged on
            throw new UnknownResourceException( "This page is not available when you are not logged on" );
        }

        User currentUser = userDAO.getUser( googleUser.getUserId() );
        if ( currentUser == null ) {
            // Should not happen. Only occurs when the user manipulates the URL or when first login (i.e. user creation in MongoDB)
            // has failed
            throw new UnknownResourceException( "This page is not available when you are not logged on" );
        }
        
        return currentUser;
    }
    
    
    /**
     * Called by dependency injection to set the user DAO on this instance.
     * 
     * @param aUserDAO
     *            the userDAO to set
     */
    public void setUserDAO( IUserDAO aUserDAO ) {
        userDAO = aUserDAO;
    }

}
