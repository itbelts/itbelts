package org.itbelts.app.controller;

import org.itbelts.dao.ICourseDAO;
import org.itbelts.domain.Course;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CourseController {
 
    private ICourseDAO courseDAO;
    
    @RequestMapping( value = "/course.htm", method = RequestMethod.GET )
    public ModelAndView showCourseDetails( @RequestParam( value="id" ) String aCourseId ) {

        Course theCourse = courseDAO.getCourse( aCourseId );

        ModelAndView model = new ModelAndView("course");
        model.addObject( "course", theCourse );
        
        return model;
    }

    /**
     * @param aCourseDAO The courseDAO to set.
     */
    public void setCourseDAO( ICourseDAO aCourseDAO ) {
        courseDAO = aCourseDAO;
    }
}
