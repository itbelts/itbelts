package org.itbelts.app.controller.form;

import java.util.List;

import org.itbelts.domain.RoleType;

/**
 * Representation for the input form for the user profile. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 13 Oct 2013 - initial release
 *
 * </pre>
 *
 * @version 13 Oct 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ProfileForm {
    
    private String name;
    private String country;
    private String email;
    private List<RoleType> roles;
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param aName the name to set
     */
    public void setName(String aName) {
        name = aName;
    }
    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }
    /**
     * @param aCountry the country to set
     */
    public void setCountry(String aCountry) {
        country = aCountry;
    }
    
    /**
     * @return Returns the email.
     */
    public String getEmail() {
        return email;
    }
    /**
     * @param aEmail The email to set.
     */
    public void setEmail( String aEmail ) {
        email = aEmail;
    }
    /**
     * @return Returns the roles.
     */
    public List<RoleType> getRoles() {
        return roles;
    }
    /**
     * @param aRoles The roles to set.
     */
    public void setRoles( List<RoleType> aRoles ) {
        roles = aRoles;
    }
    
}