package org.itbelts.app.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.itbelts.app.view.ItemLink;
import org.itbelts.app.view.TopicView;
import org.itbelts.dao.ICommunityDAO;
import org.itbelts.dao.ICourseDAO;
import org.itbelts.dao.IExamDAO;
import org.itbelts.dao.ISpecialtyDAO;
import org.itbelts.dao.ITopicDAO;
import org.itbelts.domain.Community;
import org.itbelts.domain.Course;
import org.itbelts.domain.Exam;
import org.itbelts.domain.Specialty;
import org.itbelts.domain.Topic;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ContentController {
 
    private ICommunityDAO communityDAO;
    private ICourseDAO courseDAO;
    private IExamDAO examDAO;
    private ISpecialtyDAO specialtyDAO;
    private ITopicDAO topicDAO;
    
    @RequestMapping( value = "/content.htm", method = RequestMethod.GET )
    public ModelAndView showContent( @RequestParam( value="community" ) String aCommunity ) {

        Community theCommunity = communityDAO.getCommunity( aCommunity );
        List<Course> theCourses = courseDAO.getCoursesForCommunity( aCommunity );
        List<Exam> theExams = examDAO.getExamsForCommunity( aCommunity );
        List<Specialty> theSpecialties = specialtyDAO.getSpecialtiesForCommunity( aCommunity );
        List<Topic> theTopics = topicDAO.getTopicsForCommunity( aCommunity );

        Collections.sort( theTopics, new Comparator<Topic>() {
            @Override
            public int compare( Topic aO1, Topic aO2 ) {
                return aO1.getSpecialtyId().compareTo(  aO2.getSpecialtyId() );
            }
        } );

        List<TopicView> theData = new ArrayList<TopicView>();
        for ( Topic t : theTopics ) {
            TopicView v = new TopicView();

            boolean doesTopicHaveContent = false;
            
            for ( Course c : theCourses ) {
                if ( c.getTopicId().equals( t.get_id() ) ) {
                    ItemLink l = new ItemLink();
                    
                    l.setLinkType( c.get_id().startsWith( "C" ) ? "course-link" : "workshop-link" );
                    l.setUrl( "/course.htm?id=" + c.get_id() );
                    l.setName( c.get_id().startsWith( "C" ) ? "course" : "workshop" );
                    v.getLinks().add( l );
                    
                    doesTopicHaveContent = true;
                }
            }

            for ( Exam e : theExams ) {
                if ( e.getTopicId().equals( t.get_id() ) ) {
                    ItemLink l = new ItemLink();
                    
                    l.setLinkType( "exam-link" );
                    l.setUrl( "/exam.htm?id=" + e.get_id() );
                    l.setName( "exam" );
                    v.getLinks().add( l );
                    
                    doesTopicHaveContent = true;
                }
            }
            
            // Only if the topic has meaningful content to link to --> complete its information and put it in the list
            if ( doesTopicHaveContent ) {
                v.setTopic( t );
                for ( Specialty s : theSpecialties ) {
                    if ( s.get_id().equals( t.getSpecialtyId() ) ) {
                        v.setSpecialty( s );
                        break;
                    }
                }
                theData.add( v );
            }
        }
        
        
        ModelAndView model = new ModelAndView("content");
        model.addObject( "community", theCommunity );
        model.addObject( "topics", theData );
        
        return model;
    }

    /**
     * @param aCommunityDAO The communityDAO to set.
     */
    public void setCommunityDAO( ICommunityDAO aCommunityDAO ) {
        communityDAO = aCommunityDAO;
    }

    /**
     * @param aCourseDAO The courseDAO to set.
     */
    public void setCourseDAO( ICourseDAO aCourseDAO ) {
        courseDAO = aCourseDAO;
    }

    /**
     * @param aExamDAO The examDAO to set.
     */
    public void setExamDAO( IExamDAO aExamDAO ) {
        examDAO = aExamDAO;
    }

    /**
     * @param aSpecialtyDAO The specialtyDAO to set.
     */
    public void setSpecialtyDAO( ISpecialtyDAO aSpecialtyDAO ) {
        specialtyDAO = aSpecialtyDAO;
    }
    
    /**
     * @param aTopicDAO The topicDAO to set.
     */
    public void setTopicDAO( ITopicDAO aTopicDAO ) {
        topicDAO = aTopicDAO;
    }
    
}
