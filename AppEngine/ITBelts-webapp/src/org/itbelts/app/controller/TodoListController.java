package org.itbelts.app.controller;

import java.util.List;

import org.itbelts.app.controller.business.UserLoggedOn;
import org.itbelts.dao.ICourseDAO;
import org.itbelts.dao.IExamDAO;
import org.itbelts.dao.ITodoDAO;
import org.itbelts.dao.ITopicDAO;
import org.itbelts.domain.Course;
import org.itbelts.domain.Exam;
import org.itbelts.domain.Todo;
import org.itbelts.domain.Topic;
import org.itbelts.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TodoListController {

    private ITodoDAO           todoDAO;
    private ITopicDAO          topicDAO;
    private ICourseDAO         courseDAO;
    private IExamDAO           examDAO;
    private UserLoggedOn       securityChecker;

    public static final String ALL     = "ALL";
    public static final String EXAMS   = "EXAMS";
    public static final String TOPICS  = "TOPICS";
    public static final String COURSES = "COURSES";

    @RequestMapping( value = "/todo-list.htm", method = RequestMethod.GET )
    public ModelAndView listAllTodos() {

        // Will throw exception when logon was not OK
        securityChecker.getLoggedOnUser();

        List<Todo> theTodos = todoDAO.getTodos();

        ModelAndView model = new ModelAndView( "todo-list" );
        model.addObject( "todos", theTodos );
        model.addObject( "listType", ALL );

        return model;
    }

    @RequestMapping( value = "/todo-list-for-exam.htm", method = RequestMethod.GET )
    public ModelAndView listTodosForExam( @RequestParam( value = "examId" ) String anExamId ) {

        // Will throw exception when logon was not OK
        securityChecker.getLoggedOnUser();

        List<Todo> theTodos = todoDAO.getTodosForExam( anExamId );
        Exam theExam = examDAO.getExam( anExamId );

        ModelAndView model = new ModelAndView( "todo-list" );
        model.addObject( "todos", theTodos );
        model.addObject( "listType", EXAMS );
        model.addObject( "listName", theExam.getName() );

        return model;
    }

    @RequestMapping( value = "/todo-list-for-topic.htm", method = RequestMethod.GET )
    public ModelAndView listTodosForTopic( @RequestParam( value = "topicId" ) String aTopicId ) {

        // Will throw exception when logon was not OK
        securityChecker.getLoggedOnUser();

        List<Todo> theTodos = todoDAO.getTodosForTopic( aTopicId );
        Topic theTopic = topicDAO.getTopic( aTopicId );

        ModelAndView model = new ModelAndView( "todo-list" );
        model.addObject( "todos", theTodos );
        model.addObject( "listType", TOPICS );
        model.addObject( "listName", theTopic.getName() );

        return model;
    }

    @RequestMapping( value = "/todo-list-for-course.htm", method = RequestMethod.GET )
    public ModelAndView listTodosForCourse( @RequestParam( value = "courseId" ) String aCourseId ) {

        // Will throw exception when logon was not OK
        securityChecker.getLoggedOnUser();

        List<Todo> theTodos = todoDAO.getTodosForCourse( aCourseId );
        Course theCourse = courseDAO.getCourse( aCourseId );

        ModelAndView model = new ModelAndView( "todo-list" );
        model.addObject( "todos", theTodos );
        model.addObject( "listType", COURSES );
        model.addObject( "listName", theCourse.getName() );

        return model;
    }

    @RequestMapping( value = "/todo-solve.htm", method = RequestMethod.GET )
    public ModelAndView solveTodo( @RequestParam( value = "id" ) String aTodoId ) {

        // Will throw exception when logon was not OK
        User currentUser = securityChecker.getLoggedOnUser();

        Todo theTodo = todoDAO.getTodo( aTodoId );

        if ( theTodo.getAssignedTo() != null && !theTodo.getAssignedTo().equals( currentUser.get_id() ) ) {
            // This todo has already been assigned to someone else --> just return to the list
            return listAllTodos();
        }

        if ( theTodo.getAssignedTo() == null ) {
            // First assignment to someone
            theTodo.setAssignedTo( currentUser.get_id() );
            todoDAO.saveTodo( theTodo );
        }
        ModelAndView model = new ModelAndView( "todo-solve" );
        model.addObject( "todo", theTodo );
        return model;

    }

    @RequestMapping( value = "/todo-unassign.htm", method = RequestMethod.GET )
    public ModelAndView unassignTodo( @RequestParam( value="id" ) String aTodoId, @RequestParam( value="listType" ) String aListType ) {
        
        // Will throw exception when logon was not OK
        securityChecker.getLoggedOnUser();

        Todo theTodo = todoDAO.getTodo( aTodoId );
        
        if ( theTodo.getAssignedTo() == null ) {
            // This todo has not even been assigned to someone --> just return to the list
            return listAllTodos();
        }
        
        // Remove the assignment
        theTodo.setAssignedTo( null );
        todoDAO.saveTodo( theTodo );

        if ( aListType.equals( EXAMS ) ) {
            return listTodosForExam( theTodo.getExamId() );
        }
        if ( aListType.equals( COURSES ) ) {
            return listTodosForCourse( theTodo.getCourseId() );
        }
        if ( aListType.equals( TOPICS ) ) {
            return listTodosForTopic( theTodo.getTopicId() );
        }
        return listAllTodos();
    }

    /**
     * @param aTodoDAO
     *            The todoDAO to set.
     */
    public void setTodoDAO( ITodoDAO aTodoDAO ) {
        todoDAO = aTodoDAO;
    }

    /**
     * @param aTopicDAO
     *            The topicDAO to set.
     */
    public void setTopicDAO( ITopicDAO aTopicDAO ) {
        topicDAO = aTopicDAO;
    }

    /**
     * @param aCourseDAO
     *            The courseDAO to set.
     */
    public void setCourseDAO( ICourseDAO aCourseDAO ) {
        courseDAO = aCourseDAO;
    }

    /**
     * @param aExamDAO
     *            The examDAO to set.
     */
    public void setExamDAO( IExamDAO aExamDAO ) {
        examDAO = aExamDAO;
    }

    /**
     * @param aSecurityChecker
     *            The securityChecker to set.
     */
    public void setSecurityChecker( UserLoggedOn aSecurityChecker ) {
        securityChecker = aSecurityChecker;
    }

}
