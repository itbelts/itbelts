package org.itbelts.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ThankYouController {
 
    @RequestMapping( value = "/thank-you.htm", method = RequestMethod.GET )
    public String thankYou() {
        return "thank-you";
    }
}
