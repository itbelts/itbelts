package org.itbelts.exception;

/**
 * Exception that indicates that a recource could not be found, typicalle
 * because of an incorrect (or manipulated URL)
 * 
 * @author Koen Bruyndonckx
 */
public class UnknownResourceException extends RuntimeException {

    private static final long serialVersionUID = -4452016695795614408L;

    /**
     * Simple constructor that indicates the cause of the exception.
     * 
     * @param aMessage
     */
    public UnknownResourceException(String aMessage) {
        super( aMessage );
    }
}
