package org.itbelts.dao.gae;

import java.util.List;

import org.itbelts.dao.IUserDAO;
import org.itbelts.domain.BeltInfo;
import org.itbelts.domain.RoleType;
import org.itbelts.domain.User;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

/**
 *  Implementation for IUserDAO using restful calls to MongoDB.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * 12 Oct 2013 - initial release
 *
 * </pre>
 *
 * @version 12 Oct 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class UserDAOImpl implements IUserDAO {

    private static final String USER_ID = "GaeUserId";
    private static final String USER_NAME = "name";
    private static final String USER_EMAIL = "email";
    private static final String USER_COUNTRYCD = "countryCd";
    private static final String USER_RATING = "rating";
    private static final String USER_COACHINGS = "coachings";
    private static final String USER_AUTHORITIES = "authorities";
    private static final String USER_BELTS = "belts";


    /* (non-Javadoc)
     * @see org.itbelts.dao.IUserDAO#getUser(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public User getUser(String aId) {
        Key key = KeyFactory.createKey( USER_ID, aId );
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        try {
            Entity theUserEntity = datastore.get(key);

            User theUser = new User();
            theUser.set_id( aId );
            theUser.setName( (String) theUserEntity.getProperty( USER_NAME ) );
            theUser.setEmail( (String ) theUserEntity.getProperty( USER_EMAIL ) );
            theUser.setCountryCd( (String ) theUserEntity.getProperty( USER_COUNTRYCD ) );
            theUser.setRating( ((Long) theUserEntity.getProperty( USER_RATING )).intValue() );
            theUser.setCoachings( ((Long) theUserEntity.getProperty( USER_COACHINGS )).intValue() );
            String theRoles = (String)theUserEntity.getProperty(USER_AUTHORITIES);
            if ( theRoles != null ) {
                for ( int i = 0 ; i < theRoles.length() ; i++ ) {
                    theUser.getRoles().add( RoleType.getInstanceFromCode( theRoles.substring( i, i+1 ) ) );
                }
            }
            theUser.setBelts( (List<BeltInfo>)theUserEntity.getProperty(USER_BELTS) );

            return theUser;

        } catch ( EntityNotFoundException e ) {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see org.itbelts.dao.IUserDAO#saveUser(org.itbelts.app.security.User)
     */
    @Override
    public User saveUser(User aUser) {
        Key key = KeyFactory.createKey( USER_ID, aUser.get_id());
        
        Entity userEntity = new Entity(key);
        userEntity.setProperty( USER_EMAIL, aUser.getEmail() );
        userEntity.setProperty( USER_NAME, aUser.getName() );
        userEntity.setProperty( USER_COUNTRYCD, aUser.getCountryCd() );
        userEntity.setUnindexedProperty( USER_RATING, aUser.getRating() );
        userEntity.setUnindexedProperty( USER_COACHINGS, aUser.getCoachings() );
        userEntity.setUnindexedProperty( USER_BELTS, "" );
        userEntity.setUnindexedProperty( USER_BELTS, aUser.getBelts() );

        StringBuilder b = new StringBuilder();
        for (RoleType r : aUser.getRoles()) {
            b.append( r.getCode() );
        }

        userEntity.setUnindexedProperty( USER_AUTHORITIES, b.toString() );

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put( userEntity );
        
        return aUser;
    }

}
