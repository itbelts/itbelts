$(".next-question-button").click(function () {
    $header = $(this);
    $content = $header.parent();
    $content.slideUp(200, function () {
    	$next_question = $content.next().next();
    	$next_question.slideDown(200);
    });
});