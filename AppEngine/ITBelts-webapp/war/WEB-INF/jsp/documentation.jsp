<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Documentation"/>
</jsp:include>

<body>

	<script type="text/javascript" src="http://codeorigin.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script>
	 $(function() {
		 $( "#accordion" ).accordion({
		   icons: null,
		   heightStyle: "content"
		 });
	 });
	</script>

	<div id="page">
		<div id="wrapper">

            <jsp:include page="includes/header.jsp"/>

			<div id="content">

				<div class="plain-content">
					<p></p>
					<h1>ITBelts Documentation</h1>
					<p></p>
					
				    <div id="accordion">
						<h3><div class="question-title"><p>- Welcome...</p></div></h3>
						<div class="question-content">    
							<p>Welcome to the documentation page of this site.  This will be your reference guide to all functionalities of the site.
							</p>
							<p>Click on the headers below to expand the content.
							</p>
						</div>

						<h3><div class="question-title"><p>- Concepts</p></div></h3>
						<div class="question-content">    
							<p>The content of this site is partitioned into <a href="/content-index.htm">communities</a>.  There is the "Java" community for all Java-related content
							and there is the "IT Craftsmanship" community for other content that you might find interesting as a developer.
							We will talk about belts later on, but you can earn separate belts in each community. 
							</p>
							<p>Each community is divided into <a href="/content.htm?community=J">specialties</a>.  Examples of such specialties are "Java SE" or "Java EE".
							</p>
							<p>A specialty is a grouping of similar "topics".  Each topic can have a course, a workshop and/or 1 or more exams associated with it.
							An example of a topic within the "Java SE" specialty is "Java & OO Fundamentals".
							</p>
						</div>

						<h3><div class="question-title"><p>- Belts</p></div></h3>
						<div class="question-content">    
							<p>Information to be added on belts...
							</p>
						</div>

						<h3><div class="question-title"><p>- Coaching</p></div></h3>
						<div class="question-content">    
							<p>Information to be added on coaching...
							</p>
						</div>

						<h3><div class="question-title"><p>- Contributing</p></div></h3>
						<div class="question-content">    
							<p>Information to be added on contribution...
							</p>
						</div>
					</div>
				</div>

			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>


</body>
</html>

