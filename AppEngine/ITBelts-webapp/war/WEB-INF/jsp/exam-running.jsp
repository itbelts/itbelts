<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.itbelts.app.controller.form.ExamForm" %>
<%@ page import="org.itbelts.domain.AnswerType"%>
<%@ page import="org.itbelts.domain.Exam" %>
<%@ page import="org.itbelts.domain.PossibleAnswer" %>
<%@ page import="org.itbelts.domain.Question" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Exam Running"/>
</jsp:include>
<body>
	<script type="text/javascript" src="/js/counter.js" defer="defer"></script>
	<script type="text/javascript" src="/js/keepAlive.js" ></script>
	<script type="text/javascript" src="http://codeorigin.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script>
	 $(function() {
		 $( "#accordion" ).accordion({
		   icons: null,
		   heightStyle: "content"
		 });
	 });
	</script>
	
	<div id="page">
		<div id="wrapper">
            <jsp:include page="includes/header.jsp"/>
			<div id="content">
<%
				Exam theExam = (Exam) request.getAttribute( "exam" );
				ExamForm theForm = (ExamForm) request.getAttribute( "examForm" );
			    pageContext.setAttribute("ue", theForm.getUserExamId() );
%>
				<form:form method="post" action="/exam-running.htm?ue=${ue}" modelAttribute="examForm">
				    <div class="countdown">
					    Time remaining: <span id="counter1"><%= theForm.getEndTime() %></span>
				    </div>
					<div class="plain-content">
						<p></p>
						<h1><%=theExam.getName()%></h1>
						<p></p>
						
					    <div id="accordion">
<%
    					int q_index = 0;
                		for ( Question q : theForm.getQuestions() ) {
       					    pageContext.setAttribute("qi", q_index);
%>
							<h3><div class="question-title">Question <%=q_index + 1%></div></h3>
							<div class="question-content">    
								<div class="question-description"><%=q.getDescription().replace("<font color=\"#808080\">", "<font color=\"#808080\"><br>") %></div>    
								
								<div class="question-answers">
<%
    								boolean isFirst = true;
    								int a_index = 0;
			                        for ( PossibleAnswer pa : q.getPossibleAnswers() ) {
			       					    pageContext.setAttribute("ai", a_index);
%>
					    				<div class="question-answer<%= isFirst?"-first":"" %>">
<%
                        					if ( pa.getType() == AnswerType.Text ) {
%>
												Your answer:
												<form:input path="answers[${qi}].givenAnswers[0]"></form:input>
<%
					                        } else if ( pa.getType() == AnswerType.RadioButton ) {
%>
												<form:radiobutton path="answers[${qi}].givenAnswers[0]" value="${ai}"/><%= pa.getText().replace("<font color=\"#808080\">", "<font color=\"#808080\"><br>") %>
<%
					                        } else if ( pa.getType() == AnswerType.CheckBox ) {
%>
												<form:checkbox path="answers[${qi}].givenAnswers[${ai}]" value="${ai}"/><%= pa.getText().replace("<font color=\"#808080\">", "<font color=\"#808080\"><br>") %>
<%
					                        }
%>
						    			</div>
					    			
<%
    									isFirst = false;
										a_index++;
                    					if ( pa.getType() == AnswerType.Text ) {
                    					    break;
				                        }
			                        }
%>
					    		</div>
<%
								q_index++;
								if ( q_index == theForm.getQuestions().size() ) {
%>
									<input type="submit" value="Submit answers and finish exam">
<%
								} else {
%>
			    					<input type="button" class="next-question-button" value="Answer and show next question">
<%
								}
%>
                        		<div class="question-feedback">
									<form:checkbox path="answers[${qi}].reportLayoutProblem"/>Report formatting issue
                        		</div>
								<form:input type="hidden" path="answers[${qi}].questionId"></form:input>
							</div>
<%
                        }
%>
					    </div>
					</div>
				</form:form>	
			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>
	<script type="text/javascript" src="/js/folding.js" ></script>
</body>
</html>
