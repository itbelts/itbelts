<%@ page import="org.itbelts.domain.Exam" %>
<%@ page import="org.itbelts.app.controller.business.ExamAccess" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Exam Launch"/>
</jsp:include>
<body>
	<div id="page">
		<div id="wrapper">
            <jsp:include page="includes/header.jsp"/>
<%
    		Exam theExam = (Exam) request.getAttribute( "exam" );
    		int examAccess = (Integer) request.getAttribute( "examAccess" );
    		String btnTxt = null;
    		String disabled = "";
    		if ( examAccess == ExamAccess.OK ) {
    		    if ( theExam.getContributionPoints() > 0 ) {
    				btnTxt = "Yes, please use " + theExam.getContributionPoints() + " points and proceed with the exam...";
    		    } else {
    				btnTxt = "Yes, please let me proceed with this free exam...";
    		    }
    		} else if ( examAccess == ExamAccess.CAN_RETRY ) {
    		    btnTxt = "Yes, please let me retry this exam free of charge...";
    		} else if ( examAccess == ExamAccess.ALREADY_PASSED ) {
    		    btnTxt = "Oops, you already passed this exam.";
    		    disabled = "disabled=\"disabled\"";
    		}
%>
			<div id="content">
				<div class="plain-content">
					<p></p>	
					<h1><%= theExam.getName() %></h1>
					<p>&nbsp;</p>
				    <h2>You are starting a real exam.</h2>
				    <p>Unless you are retrying this exam free of charge, <%= theExam.getContributionPoints() %> contribution points will be deducted from your balance when you proceed.</p>
				    <p>For this exam you can use any reference material you see fit...</p>
				    e.g.: <ul>
				        <li>Your favourite IDE
				        <li>Browser
				        <li>Books
				    </ul>
				    <p>( Keep in mind that you will typically not have enough time to look up every answer within the given time frame. )</p>
				    <p>&nbsp;</p>
				    <p>&nbsp;</p>
				    <table>
				      <tr>
				        <td style="width:30%;text-align: left"><button onclick="window.location = '/exam.htm?id=<%= theExam.get_id() %>' ">No thanks, please take me back to the exam details</button></td>
				        <td style="width:10%;">&nbsp;</td>
				        <td style="width:30%;text-align: right"><button onclick="window.location = '/exam-running.htm?id=<%= theExam.get_id() %>' " <%= disabled %>><%= btnTxt %></button></td>
				        <td style="width:30%;">&nbsp;</td>
				      </tr>    
				    </table>
				</div>
			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>
</body>
</html>
