<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Content"/>
</jsp:include>

<body>

	<div id="page">
		<div id="wrapper">

            <jsp:include page="includes/header.jsp"/>

			<div id="content">

				<div id="features">
					<h1>ITBelts content</h1>

					<ul class="feature-list">
						<li class="feature-item clearfix">
							<div class="feature-image">
								<a href="/content.htm?community=J"><img src="/img/java-content.png"></a>
							</div>
							<div class="feature-text">
								<h2>
									<a href="/content.htm?community=J">Java related content</a>
								</h2>

								<p>We have a comprehensive set of Java-related material.</p>

							</div>
						
						<li class="feature-item clearfix">
							<div class="feature-image">
								<a href="/content.htm?community=I"><img src="/img/craftsmanship-content.png"></a>
							</div>
							<div class="feature-text">
								<h2>
									<a href="/content.htm?community=I">IT Craftsmanship related content</a>
								</h2>

								<p>And a whole offering of material related to IT
									Craftsmanship. Various techniques an IT professional could be
									interested in.</p>

							</div>
						
					</ul>
				</div>

			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>


</body>
</html>
