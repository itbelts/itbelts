<%@ page import="org.itbelts.domain.Exam" %>
<%@ page import="org.itbelts.domain.RoleType" %>
<%@ page import="org.itbelts.domain.User" %>
<%@ page import="org.itbelts.app.controller.business.ExamAccess" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Exam"/>
</jsp:include>

<body>
	<div id="page">
		<div id="wrapper">
            <jsp:include page="includes/header.jsp"/>
<%
    		Exam theExam = (Exam) request.getAttribute( "exam" );
            User theUser = (User) request.getAttribute( "user" );
%>
			<div id="content">
			    <div class="col75">
					<div class="plain-content">
						<p></p>
						<h1><%= theExam.getName() %></h1>
						<p></p>
					    <%= theExam.getDescription() %>
					</div>
				</div>
			    <div class="col25">
			    	<div class="launch-box">
						<p>This exam is <%= theExam.getStatusDescription() %></p>
						
						<p>Contribution points: <%= theExam.getContributionPoints() %><br>
						   Knowledge points: <%= theExam.getKnowledgePoints() %>
<%
						if ( theUser != null ) {
	                        int todosAvailable = (Integer) request.getAttribute( "todosAvailable" );
				    		if ( todosAvailable > 0 ) {
%>
								<br>Open todos: <%= todosAvailable %> <a href="/todo-list-for-exam.htm?examId=<%= theExam.get_id() %>">Show me</a>
<%
				    		}
			    		}
%>
						</p>
<%
                        String disabled = "";
			    		if ( theUser == null ) {
	                        disabled = "disabled=\"disabled\"";
%>
							<p>&nbsp;</p>
							<div class="smalltext"><p>You are not logged on.  Unable to verify if you can afford to take this exam.</p></div>
<%
			    		} else {
            				int examAccess = (Integer) request.getAttribute( "examAccess" );
            				if ( examAccess == ExamAccess.OK ) {
				    		    // OK
%>
								<p>&nbsp;</p>
								<p>&nbsp;</p>
<%
				    		} else if ( examAccess == ExamAccess.NOT_ENOUGH_POINTS ) {
	            	            disabled = "disabled=\"disabled\"";
%>
								<div class="smalltext"><p>You do not have enough contribution points to take this exam.  Refer to the documentation to see how you can earn points.</p></div>
<%
			    			} else if ( examAccess == ExamAccess.ALREADY_PASSED ) {
	                        	disabled = "disabled=\"disabled\"";
%>
								<div class="smalltext"><p>You already passed this exam.</p></div>
<%
			    			}
			    		}
%>
						<form action="/exam-launch.htm?id=<%= theExam.get_id() %>" method="post">
							<input type="submit" value="Take this exam" style="width:100%" <%= disabled %>>
						</form>
						<p>&nbsp;</p>
						<p><a href="/documentation.htm">What is a an exam?</a></p>
			    		<p><a href="#" onclick="history.back();">Back to overview</a></p>
			    	</div>
				</div>
			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>
</body>
</html>
