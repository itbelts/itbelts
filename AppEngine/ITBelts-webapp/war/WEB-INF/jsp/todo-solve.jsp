<%@ page import="java.util.List" %>
<%@ page import="org.itbelts.domain.Todo" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Solving a TODO"/>
</jsp:include>

<body>

	<div id="page">
		<div id="wrapper">

            <jsp:include page="includes/header.jsp"/>

			<div id="content">

				<div class="plain-content">
					<p></p>
					<h1>Thanks for your support.  This todo has now been assigned to you.</h1>
					<p></p>
					You can now proceed to the content and start helping us to fix it.
					<p></p>
					
					<%
					    Todo t = (Todo) request.getAttribute( "todo" );
					    if ( t.getQuestionId() != null ) {
					%>
							<button onclick="window.location = '/question.htm?id=<%= t.getQuestionId() %>' ">Update question <%= t.getQuestionId() %></button>
					<%            
				        } else {
						    if ( t.getCourseId() != null ) {
					%>
								<button onclick="window.location = '/course.htm?id=<%= t.getCourseId() %>' ">Update course <%= t.getCourseId() %></button>
					<%            
					        } else {
							    if ( t.getTopicId() != null ) {
					%>
									<button onclick="window.location = '/topic.htm?id=<%= t.getTopicId() %>' ">Update topic <%= t.getTopicId() %></button>
					<%            
						        } else {
								    if ( t.getExamId() != null ) {
					%>
										<button onclick="window.location = '/exam.htm?id=<%= t.getExamId() %>' ">Update exam <%= t.getExamId() %></button>
					<%            
							        } else {
					%>
					                    <td>&nbsp;</td>
					<%            
							        }
						        }
					        }
				        }
					%>
				</div>

			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>


</body>
</html>
