<%@ page import="org.itbelts.domain.Course" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Course Content"/>
</jsp:include>

<body>
	<div id="page">
		<div id="wrapper">
            <jsp:include page="includes/header.jsp"/>
<%
    Course theCourse = (Course) request.getAttribute( "course" );
%>
			<div id="content">
				<div class="plain-content">
					<p></p>
					<h1><%= theCourse.getName() %></h1>
					<p></p>
				    Here we show the content of the course itself?  How?  In a frame pointing to external content?  Chapter per chapter?  TBD :)
				</div>
			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>
</body>
</html>
