package org.itbelts.admin.mongolab;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import org.itbelts.dao.mongolab.AbstractMongoLabDAO;
import org.itbelts.dao.mongolab.CourseDAOImpl;
import org.itbelts.domain.Course;
import org.itbelts.domain.CourseStatus;

public class CourseLoader extends AbstractMongoLabDAO {

    private static final Logger LOGGER = Logger.getLogger( CourseLoader.class.getName() );

    private static final String PATH   = "collections/courses";
    
    /**
     * @param args
     */
    public static void main( String[] args ) throws Exception {
        new CourseLoader().load();
        LOGGER.info( new CourseDAOImpl().getCourses().toString() );
    }

    private void load() {
        
        sendTo( buildUrl( PATH ), new Course( "J0101", "C001", "Java and OO Fundamentals", getDescriptionFor( "C001" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0101", "W001", "Java and OO Fundamentals Workshop", getDescriptionFor( "W001" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0102", "C002", "Java and OO Intermediate", getDescriptionFor( "C002" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0102", "W002", "Java and OO Intermediate Workshop", getDescriptionFor( "W002" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0103", "C003", "Java Advanced", getDescriptionFor( "C003" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0105", "C004", "Java SE Collections", getDescriptionFor( "C004" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0120", "C005", "Java 7 New Language Features", getDescriptionFor( "C005" ) ));

        sendTo( buildUrl( PATH ), new Course( "J0206", "C006", "EJB 3.1 Fundamentals", getDescriptionFor( "C006" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0210", "C007", "Servlets 3", getDescriptionFor( "C007" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0210", "W007", "Servlets 3 Workshop", getDescriptionFor( "W007" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0213", "C008", "JSP and Expression language", getDescriptionFor( "C008" ) ));

        sendTo( buildUrl( PATH ), new Course( "J0301", "C009", "Spring Core Fundamentals", getDescriptionFor( "C009" ) ));

        sendTo( buildUrl( PATH ), new Course( "J0404", "C010", "JPA/Hibernate Fundamentals", getDescriptionFor( "C010" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0404", "W010", "JPA/Hibernate Fundamentals Workshop", getDescriptionFor( "W010" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0405", "C011", "JPA/Hibernate Lifecycle", getDescriptionFor( "C011" ) ));
        sendTo( buildUrl( PATH ), new Course( "J0406", "C012", "JPA/Hibernate Mapping", getDescriptionFor( "C012" ) ));

        sendTo( buildUrl( PATH ), new Course( "J0501", "C013", "Vaadin Fundamentals", getDescriptionFor( "C013" ) ));
        
        // No relevant content in this course
        Course c = new Course( "J0502", "C014", "Vaadin Core Advanced", getDescriptionFor( "C014" ) );
        c.setStatus( CourseStatus.beta );
        sendTo( buildUrl( PATH ), c );
        

        // No relevant content in this course
        c = new Course( "J0509", "C015", "JSF 1.1 - Basic", getDescriptionFor( "C015" ) );
        c.setStatus( CourseStatus.beta );
        sendTo( buildUrl( PATH ), c );

        sendTo( buildUrl( PATH ), new Course( "I0401", "C016", "Unix Fundamentals", getDescriptionFor( "C016" ) ));

        sendTo( buildUrl( PATH ), new Course( "I0601", "C017", "Python", getDescriptionFor( "C017" ) ));
        sendTo( buildUrl( PATH ), new Course( "I0602", "C018", "GO", getDescriptionFor( "C018" ) ));
        
        // No relevant content in this course
        c = new Course( "I0603", "C019", "Clojure", getDescriptionFor( "C019" ) );
        c.setStatus( CourseStatus.beta );
        sendTo( buildUrl( PATH ), c );
        
        // No relevant content in this course
        c = new Course( "I0605", "C020", "Grails Fundamentals", getDescriptionFor( "C020" ) );
        c.setStatus( CourseStatus.beta );
        sendTo( buildUrl( PATH ), c );
        
        sendTo( buildUrl( PATH ), new Course( "I0608", "C021", "Javascript", getDescriptionFor( "C021" ) ));

        // No relevant content in this course
        c = new Course( "M0101", "C023", "iPhone Programming Fundamentals", getDescriptionFor( "C023" ) );
        c.setStatus( CourseStatus.beta );
        sendTo( buildUrl( PATH ), c );
        
        sendTo( buildUrl( PATH ), new Course( "M0102", "C024", "Android Fundamentals", getDescriptionFor( "C024" ) ));

        sendTo( buildUrl( PATH ), new Course( ".0101", "C022", "C# and OO Fundamentals", getDescriptionFor( "C022" ) ));
    }

    /**
     * Load the description from an XML
     * 
     * @param aString
     * @return
     */
    private String getDescriptionFor( String aString ) {
        StringBuilder s = new StringBuilder();
        
        try {
            BufferedReader r = new BufferedReader( new InputStreamReader( aString.getClass().getResourceAsStream( "/" + aString + ".txt" ) ) );
            String theLine;
            while ( ( theLine = r.readLine() ) != null ) {
                s.append( theLine );
                s.append( "<br>" );
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        return s.toString();
    }

}
