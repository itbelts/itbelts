package org.itbelts.admin.mongolab;

import java.util.List;
import java.util.logging.Logger;

import org.itbelts.dao.mongolab.AbstractMongoLabDAO;
import org.itbelts.dao.mongolab.CommunityDAOImpl;
import org.itbelts.domain.Community;

/**
 * Loader for the set of communities in the system.
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 19-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      19-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class CommunityLoader extends AbstractMongoLabDAO {

    private static final Logger LOGGER = Logger.getLogger( CommunityLoader.class.getName() );
    
    private static final String PATH = "collections/communities";
    
    /**
     * @param args
     */
    public static void main( String[] args ) throws Exception {
        new CommunityLoader().load();
    }

    private void load() throws Exception {
        sendTo( buildUrl( PATH ), new Community( "J", "Java" ) );
        sendTo( buildUrl( PATH ), new Community( "D", "DB" ) );
        sendTo( buildUrl( PATH ), new Community( "I", "IT Craftsmanship" ) );
        sendTo( buildUrl( PATH ), new Community( "M", "Mobile" ) );
        sendTo( buildUrl( PATH ), new Community( ".", ".Net" ) );
        sendTo( buildUrl( PATH ), new Community( "U", "ITBelts" ) );

        List<Community> theCommunities = new CommunityDAOImpl().getCommunities(); 
        
        LOGGER.info( theCommunities.toString() );

    }

}
