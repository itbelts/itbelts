This workshop gives you extensive practice on the topics covered in the "Java and OO Fundamentals - Course". It helps you assimilate the concepts.

You will be asked to develop small projects from scratch to finish, while being guided by your coach. 
The small projects bring you a step closer to real projects and are more difficult than the small exercises contained in each chapter of the course.

In order to build the solution to the projects you will be required to mix your knowledge gained during the various chapters of the corresponding course.

The workshop is ideally suited if you:

<ul>
<li>want to move on to the next course before being involved on a real project
<li>need a challenge to figure out parts of the course you may have missed are not currently on a project where you can utilize the skills gained in the course
</ul>

If you did the course, but have difficulties to pass the exam, the workshop will help you reach the objective.