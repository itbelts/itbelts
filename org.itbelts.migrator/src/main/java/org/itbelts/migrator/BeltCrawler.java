package org.itbelts.migrator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.itbelts.domain.Belt;
import org.itbelts.domain.BeltInfo;
import org.itbelts.domain.User;

import com.thoughtworks.xstream.XStream;

/**
 * Parser for user and belt information. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 * 
 * </pre>
 * 
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class BeltCrawler extends AbstractCrawler {

    /**
     * Find all files ending in "Belts.htm" and parse them.
     * 
     * @param aBaseFolder
     * @param aDestinationFolder
     */
    public void doProcessing( File aBaseFolder, String aDestinationFolder ) {

        Map<String, User> theUserMap = new TreeMap<String, User>();

        for ( String theFile : aBaseFolder.list() ) {
            if ( !theFile.endsWith( "Belts.htm" ) ) {
                continue;
            }

            String[] theParts = theFile.split( " " );
            String theColor = theParts[ 1 ];
            String theCommunity = theParts[ 0 ];
            System.out.println( "Processing " + theColor + " in the " + theCommunity + " community..." );

            StringBuilder theBuffer = loadFile( aBaseFolder, theFile );

            // First skip some of the header stuff (because the current user is also in there)
            String theUsers = theBuffer.substring( theBuffer.indexOf( "GroupsAndUsers" ) );

            // Then find the first user in the list
            String theUserStart = "<a href=\"#%21User/";
            theUsers = theUsers.substring( theUsers.indexOf( theUserStart ) );

            theUsers = theUsers.substring( 0, theUsers.indexOf( "v-verticallayout-BAKG_DEFAULT BAKG_DEFAULT" ) );

            theUsers = theUsers.replaceAll( theUserStart, "\n" + theUserStart );

            String[] theLines = theUsers.split( "\n" );

            for ( String theUserLine : theLines ) {
                if ( theUserLine.trim().length() == 0 ) {
                    continue;
                }

                // Skip the href link
                theUserLine = theUserLine.substring( theUserStart.length() );

                // Get the id (= all until next double quote)
                String theId = theUserLine.substring( 0, theUserLine.indexOf( '"' ) );

                // Get the user for that id or create a new one for a new id
                User theUser = theUserMap.get( theId );
                if ( theUser == null ) {
                    theUser = new User();
                    theUser.set_id( theId );
                    theUserMap.put( theId, theUser );
                }

                // Skip the Id, the ending quote and the '>'
                theUserLine = theUserLine.substring( theId.length() + 2 );

                String theName = theUserLine.substring( 0, theUserLine.indexOf( "</a>" ) );
                theName = theName.replace( "<b>", "" );
                theName = theName.replace( "</b>", "" );
                theUser.setName( theName );

                int theRatingPos = theUserLine.indexOf( "; height: 11px;\" class=\"v-ratingstars-bar\"" );
                if ( theRatingPos >= 0 ) {
                    int theRating = Integer.parseInt( theUserLine.substring( theRatingPos - 4, theRatingPos - 1 ).trim() );
                    theUser.setRating( theRating );
                }

                int theCoachingsPos = theUserLine.indexOf( "smallerText\">(" );
                if ( theCoachingsPos >= 0 ) {
                    int theCoachings = Integer.parseInt( theUserLine.substring( theCoachingsPos + 14, theUserLine.indexOf( ")", theCoachingsPos ) ).trim() );
                    theUser.setCoachings( theCoachings );
                }

                BeltInfo theInfo = new BeltInfo();
                theInfo.setLevel( Belt.valueOf( theColor ) );
                theInfo.setCommunity( theCommunity );
                theUser.getBelts().add( theInfo );

                if ( theUserLine.indexOf( "_files/" ) >= 0 ) {
                    // Skip everything upto the country image
                    theUserLine = theUserLine.substring( theUserLine.indexOf( "_files/" ) + 7 );

                    String theCountryCd = theUserLine.substring( 0, theUserLine.indexOf( "." ) );

                    theUser.setCountryCd( theCountryCd );
                }

            }

        }

        XStream x = new XStream();
        try {
            BufferedWriter w = new BufferedWriter( new FileWriter( new File( aDestinationFolder + "users.xml" ) ) );
            w.write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" );
            w.write( CRLF );

            for ( Map.Entry<String, User> e : theUserMap.entrySet() ) {
                w.write( x.toXML( e.getValue() ) );
                w.write( CRLF );
            }

            w.flush();
            w.close();
        } catch ( IOException exc ) {
            exc.printStackTrace( System.err );
        }
    }

}
