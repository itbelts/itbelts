<topic>
<title>Spring Core Fundamentals - Workshop</title>
<para>This workshop guides you through the creation of small applications to put into practice all the things you've seen in the Spring Core Fundamentals course. That way, you'll assimilate how everything works together.</para>
<para>For each application, create a new Eclipse project with the appropriate structure.</para>
<topic>
<title>Managing Users</title>
<para>In this first workshop exercise you are about to create a User/Status management system using the Spring framework.</para>
<topic>
<title>Setup</title>
<para>Start by configuring your environment</para>
<ul>
    <li>Configure a basic ApplicationContext</li>
    <li>Set up a HSQLDB data source.</li>
</ul>
</topic>
<topic>
<title>Basic Persistence</title>
<para>Use JdbcTemplate in a UserDAO to populate the DB with User objects. Define yourself what composes a User.</para>
<para>Optional: take console input to add Users. cf. <a href="http://java.sun.com/javase/6/docs/api/java/io/Console.html">Console</a> or <a href="http://java.sun.com/javase/6/docs/api/java/util/Scanner.html">Scanner</a> for the Java 1.5+ ways of reading console input.</para>
<para>Inject the DAO class into a newly created Service class.</para>
</topic>
<topic>
<title>I18N</title>
<para>Configure i18n with a MessageSource and use an i18nService to format data extracted from DB.</para>
<para>Let the user choose a Locale when he is created.</para>
</topic>
<topic>
<title>JPA</title>
<para>Create a StatusUpdate domain class and a StatusUpdateDAO that uses an EntityManager. Each StatusUpdate contains a message a reference to a User.</para>
<para>Migrate the UserDAO to EntityManager.</para>
<para>Add a collection of status updates to the User class.</para>
<para>Unit test the retrieval of a User in the UserDAO by using a stub EntityManager implementation. cf. Martin Fowler's <a href="http://martinfowler.com/articles/mocksArentStubs.html">Mocks Aren't Stubs</a> </para>
</topic>
<topic>
<title>AOP</title>
<para>Create a logging aspect that traces every call.</para>
<para>Create an aspect that prints out a congratulations message every time a user has sent 10 status updates.</para>
<para>Use transactions to handle invalid input (eg. update User's status update counter, then fail status update insert by throwing an exception, make sure counter increment is rolled back)</para>
<para>Use a @Secured annotation that grants access to the class only to logged-in users. Try it with prototype scope and BeanPostProcessor, then with an aspect. You might also need a ThreadLocal.</para>
<para>Optional: Create an aspect that sends the StatusUpdate to Twitter. Look for a Java library that does this to help you out. Lots exist.</para>
</topic>
<topic>
<title>Optional</title>
<para>The following subjects aren't covered in the course, but are interesting to do if you have the time.</para>
<para><b>@Configurable</b></para>
<para>Use the @Configurable annotation to get rid of the UserDAO and have instance methods such as User.save(), User.delete(), etc. and static methods such as User.get(Long), User.findByName(). cf. <a href="http://static.springsource.org/spring/docs/2.5.x/reference/aop.html#aop-atconfigurable">Using AspectJ to dependency inject domain objects with Spring</a>.</para>
</topic>
</topic>
<topic>
<title>Fast Food</title>
<para>In this workshop exercise we are going to build a FastFood to produce burgers (yum yum). There are 3 main iterations:</para>
<ul>
    <li>Build and wire the classes</li>
    <li>Add JPA support</li>
    <li>Add auditing via AOP</li>
</ul>
<para>Let's start by building the domain objects.</para>
<topic>
<title>Classes and Responsibilities</title>
<para><b>BurgerType</b></para>
<para>BurgerType is an enumeration that describes the various kind of burgers we produce.</para>
<para>Enum values could be:</para>
<ul>
    <li>FISH_FILET</li>
    <li>CHICKEN_SUPREME</li>
    <li>ROYAL_WITH_CHEESE</li>
    <li>SUPREME_POUNDER</li>
    <li>BACON_QUARTER_POUNDER</li>
</ul>
<para><b>FastFood</b></para>
<para>FastFood is an interface that defines 2 public methods</para>
<ul>
    <li>public Burger deliverBurger(BurgerType) : based on a given BurgerType it create a Burger object instance</li>
    <li>public List<Burger> prepareOrder() : returns a list of Burger objects</li>
</ul>
<para>more details on method implementation in FastFoodImpl</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/12302337/fastfood.jpg'/></div><span style='color:red;'>Format error: This tag is empty, it has no name. You can either enclose your text in between [CODE escape='true' inline='true'] and [/CODE] or else you can escape your brackets with html '&amp;#91;' and '&amp;#93;' escape characters (respectively for '[' and ']' characters).</span>
<para><b>FastFoodImpl</b></para>
<para>FastFoodImpl is the central class of our application. It is injected with</para>
<ul>
    <li>a SecretRecipeBook</li>
    <li>a CookTeam</li>
</ul>
<para>In the public Burger deliverBurger(BurgerType)method you will:</para>
<ul>
    <li>Get the list of needed ingredients and their quantities using the recipe book</li>
    <li>Ask the CookTeam to prepare the burger</li>
</ul>
<para>In the public List<Burger> prepareOrder() method you will parse a burger.txt file containing a description of the Order. The format simply is BURGER_TYPE amount. Ex:</para>
<para>         FISH_FILET 1</para>
<para>         CHICKEN_SUPREME 2</para>
<para>         ROYAL_WITH_CHEESE 1</para>
<para>         SUPREME_POUNDER 5</para>
<para>         BACON_QUARTER_POUNDER 6</para>
<para>Note: use your enum's built-in valueOf() method to convert a String to an enum instance.</para>
<para>You also delegate the burger creation to the deliverBurger(BurgerType) method. Meaning if the file contains CHICKEN_SUPREME 2 you will have to call deliverBurger() twice with BurgerType.CHICKEN_SUPREME as argument.</para>
<para>Make sure your FastFoodImpl contains a Log4j Logger instance that prints something to the standard output each time a burger is requested and delivered.</para>
<para><b>SecretRecipeBook</b></para>
<para>The SecretRecipeBook is used by the FastFoodImpl to know the the amount of ingredients needed for a given burger type:</para>
<para>        public Map<Ingredient, Integer> getIngredientsAndQuantities(BurgerType)</para>
<para>This internal implementation is left up to you.</para>
<para><b>Ingredient</b></para>
<para>Ingredient is a simple class defining the set of ingredients used to cook a burger. It simply contains:</para>
<ul>
    <li>String : Type</li>
    <li>String : Description</li>
</ul>
<para><b>CookTeam</b></para>
<para>The CookTeam is responsible for cooking the burger. It is injected with StockService and OrderService (see below). The main business method of this class is:</para>
<para>    public Burger prepareBurger(Map<Ingredients, Integer>, BurgerType type)</para>
<para>The CookTeam will get the ingredients using the StockService class. If unfortunately the StockService does not contain enough ingredients the CookTeam will receive en Exception (see StockService class description) and is responsible for adding the necessary Order in the OrderService (see below).  </para>
<para><b>StockService</b></para>
<para>StockService exposes this method</para>
<para>    public void verifyAndUpdateStock(Map<Ingredient, Integer>) throws a NotEnoughIngredientsException()</para>
<para>For the moment the internal implementation of the class is left up to you.</para>
<para><b>NotEnoughIngredientsException</b></para>
<para>A checked exception that contains the missing ingredients, can obtain the missing ingredients by calling</para>
<para>   public Map<Ingredient, Integer> getMissingIngredient()</para>
<para><b>OrderService</b></para>
<para>The OrderService remembers what will be in the next Order, the class exposes 2 methods:</para>
<ul>
    <li>public void addToNextOrder(Map<Ingredient, Integer>)</li>
    <li>public void placeOrder()</li>
</ul>
<para>For the moment the implementation is kept simple</para>
<ul>
    <li>addToNextOrder simply adds to an internal Map the amount of ingredients for the next order.</li>
    <li>placeOrder uses a Logger to output the order content on the standard output.</li>
    <li></li>
</ul>
<para>Now that you have discovered the main classes write and wire them using Spring. You can use whatever method you prefer (pure xml bean defnintions or annotations)</para>
<para>When it is done, create an executable class (with a public static void main(String[] args) method) that fetches the FastFood bean and prepare the requested order.</para>
</topic>
<topic>
<title>Adding JPA</title>
<para>New requirement: we want to add database support for our application using JPA. </para>
<para><b>Ingredients</b></para>
<para>Ingredients are now entities. Even though the type field could have been a good primary key candidate, we'll add an id field of type Long instead.</para>
<para><b>Recipe and RecipeBook</b></para>
<para>RecipeBook now returns Recipe (a new class) entities that come from the database. The Recipe class contains:</para>
<ul>
    <li>an id field of type Long</li>
    <li>a description field of type String</li>
    <li>a Map<Ingredient, Integer> (see Hibernate Extensions for help). If you don't feel comfortable with mapping a Map use a Set<Ingredient></li>
</ul>
<para>Note: The CookTeam's prepareBurger() method is adapted to take a Recipe as argument:</para>
<para>    public Burger prepareBurger(Recipe recipe)</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/12302337/fastfood-jpa-changes.jpg'/></div>
<para><b>Stock Management</b></para>
<para>The Stock status is now held in the database, a new class called StockItem is created. StockItem contains:</para>
<ul>
    <li>an Ingredient</li>
    <li>a quantity (Integer)</li>
</ul>
<para><b>Order Management</b></para>
<para>Orders are also stored in the database. Create a new Order entity that contains</para>
<ul>
    <li>an id</li>
    <li>a set of Ordelines</li>
</ul>
<para>An Orderline object contains</para>
<ul>
    <li>an Ingredient</li>
    <li>a quantity (Integer)</li>
</ul>
<para><b>DAOs</b></para>
<para>After having created the entities, create the related DAOs and inject them into the corresponding services.</para>
<para>When done, test your program again.</para>
</topic>
<topic>
<title>IRSService</title>
<para>The IRS pays special attention to our FastFood and want to be alerted each and every time a burger is delivered.</para>
<para>Write a new IRSService class and an aspect that triggers the IRSService.burgerDelivered(Burger burger), this method could call a remote service to really warn the IRS but we are simply going to use a Logger to print some text on the standard output.</para>
</topic>
<topic>
<title>AuditService</title>
<para>New requirement: it seems that the Stock items disappear faster than the burgers are produced! Auditing is needed. We need to trace any access to the SecretRecipeBook and the Stock.</para>
<para>The AuditDAO class defines a void addAuditTrace(AuditTrace trace) method that will store an AuditTrace in the database.</para>
<para>An AuditTrace entity is composed of</para>
<ul>
    <li>An id of type Long</li>
    <li>A category enum (STOCK, RECIPE_BOOK, ...)</li>
    <li>A description of type String</li>
    <li>A TimeStamp to record when the event occurred</li>
</ul>
<para>Furthermore, once an AuditTrace has been recorded, it wouldn't make sense for it to be modified. Make the AuditTrace class immutable. Remember that constructors can be protected.</para>
<para>Use an aspect that delegates to the AuditTraceDAO to make sure that auditing can be easily switched on and off.</para>
</topic>
</topic>
</topic>
