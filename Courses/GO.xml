<topic>
<title>Go</title>
<topic>
<title>Beginning Go</title>
<para>This chapter will show you what Go is, its - short - history and will help you to install the software required for this course.</para>
<topic>
<title>What is Go</title>
<para>Go programming language is developed by Google Inc. </para>
<para>It is an open source project with its first stable version 1 released on 28 March  2012.</para>
<para>Go's compiler 'gc' officially targets FreeBSD, Linux, Mac OS X (Snow Leopard/Lion), and Windows operating systems and the 32-bit (386) and 64-bit (amd64) x86 processor architectures with its binary distributions. </para>
<para>One main goal of Go is to combine the efficiency of statically typed compiled languages with the convenience of </para>
<para>dynamic programming languages.</para>
<para>It is desgined for multicore and networked machines and provides easy to use concurrency mechanisms.</para>
<para>Go does not have a VM, it compiles to machine code.</para>
<topic>
<title>History</title>
<para>Robert Griesemer, Rob Pike, and Ken Thompson started Go in September 2007.</para>
<para>In November 2009 it was officially announced by Rob Pike in a Google TechTalk. </para>
<para>Rob Pike publicly stated about Go "This is real stuff, and we're using it." in an interview with <a href="http://www.theregister.co.uk/2010/05/20/go_in_production_at_google/">The Register</a> in May 2010.</para>
<para>On March 28, 2012 stable version 1 was released.</para>
</topic>
<topic>
<title>Intention and Goals</title>
<para>Why a new language after over a decade? </para>
<para>What was the need of Go? </para>
<para>Why did Google bother to create a new language? </para>
<para>These are the few questions that everyone of us must be having with regard to the Go. And they are obvious questions as well. Let us try to answer this questions.</para>
<para>As per the Go's official <a href="http://golang.org/doc/go_faq.html">FAQs</a>, the reason that led to the thought to create a new language are:-</para>
<ul>
<li>Computers are enormously quicker but software development is not faster.</li>
<li>Dependency management is a big part of software development today but the “header files” of languages in the C tradition are antithetical to clean dependency analysis—and fast compilation.</li>
<li>There is a growing rebellion against cumbersome type systems like those of Java and C++, pushing people towards dynamically typed languages such as Python and JavaScript.</li>
<li>Some fundamental concepts such as garbage collection and parallel computation are not well supported by popular systems languages.</li>
</li>The emergence of multicore computers has generated worry and confusion.</li>
</ul>
<para>The advantages of a Go, again as mentioned in the Go's official FAQs are:-</para>
<ul>
<li>It is possible to compile a large Go program in a few seconds on a single computer.</li>
<li>Go provides a model for software construction that makes dependency analysis easy and avoids much of the overhead of C-style include files and libraries.</li>
<li>Go's type system has no hierarchy, so no time is spent defining the relationships between types. Also, although Go has static types the language attempts to make types feel lighter weight than in typical OO languages.</li>
<li>Go is fully garbage-collected and provides fundamental support for concurrent execution and communication.</li>
<li>By its design, Go proposes an approach for the construction of system software on multicore machines.</li>
</ul>
</topic>
</topic>
<topic>
<title>Getting started</title>
<topic>
<title>Setting up</title>
<para>First you should download and install the Go compilers, tools, and libraries. </para>
<para>Go is open source and under a BSD-style license.</para>
<para>There are two official Go compiler available. </para>
<para>The gc Go compiler is more sophisticated and tested.</para>
<para>We will talk in this chapter about installing the binary distribution of the gc compiler.</para>
<para>It is available for Linux, FreeBSD, Mac OS X (Snow Leopard/Lion), Windows operating systems and the 32-bit (386) and 64-bit (amd64) x86 processor architectures</para>
<para>The gccgo compiler is part of the GNU C Compiler (GCC). You can find more information about installing it <a href="http://golang.org/doc/install/gccgo">here (installing gccgo at golang.org)</a></para>
<topic>
<title>Install Go compiler, tools and libraries</title>
<para>Go to <a href="http://golang.org/doc/install">golang.org install instructions</a> for the installation guide.</para>
<para>If you install the Go binary distribution not in the default location /usr/local/go (or c:\Go under Windows), you will need to set the GOROOT environment path to the choosen directory. </para>
<para>Windows:</para>
<para>Two different ways to install Go are provided for Windows:</para>
<para>1. zip archive: requires you to set the environment variables </para>
<para>2. MSI installer: configures the installation automatically but is still experimental. </para>
<para>All platforms</para>
<para>To test your installation you should write a small program by creating a file "installed.go" with following code:</para>
<para><code type='display'>package main</para>
<para>import "fmt"</para>
<para>func main() {</para>
<para>    fmt.Printf("Go is working\n")</para>
<para>}</code></para>
<para>Now run it:</para>
<para><code type='display'>$ go run installed.go</para>
<para>Go is working</para>
<para></code></para>
<para>If you see the "Go is working" message then your Go installation is - as it says - working. </para>
<para>And isn't that much better than a "hello world"?</para>
<para>If you're facing problems running that program, feel free to contact your coach.</para>
</topic>
</topic>
<topic>
<title>Some first and quick facts</title>
<para>Here some first facts about Go syntax.</para>
<para>Don't get lost, we will discuss all of it in detail later.</para>
<ul>
    <li>The code starts with a "package" line.
    <li>Program starts with the "main()" function in the package "main".</li>
    <li>Line-ending semicolons are only optional.</li>
    <li>Type conversions must be made explicit.</li>
    <li>Functions need the keyword "func".</li>
    <li>Functions can have more than one return value. They can be named and used like normal variables.</li>
    <li>Go does not know classes.</li>
    <li>You can and will define your own data types with the keyword "struct".</li>
    <li>Structs can have fields and methods.</li>
    <li>Go has no explicit visibility modifiers like "private" or "public". This is done implicit by capitalization.</li>
    <li>Go has no "while" but several ways of using "for" even with automatic iterators.</li>
    <li>Concurrency is done with the keyword "go". For example "go hello()" would start func hello() in its own thread.</li>
    <li>Threads are called "Go routines"</li>
</ul>
</topic>
</topic>
</topic>
<topic>
<title>Language Basics</title>
<para>This chapter covers the core Go language elements like variables, operators and control flow. </para>
<para>You will learn to write simple programs and get used to the Go syntax.</para>
<topic>
<title>Go Declaration Syntax</title>
<para>The declaration in Go is written the way you read, from left to right.</para>
<para>Some examples:</para>
<para><code type='display'>x int</para>
<para>z bool</para>
<para>a, b, c float32</para>
<para>d [5]int</para>
<para></code></para>
<para>variable x is an int, z a bool, a,b,c are all float32 and d is an array of int with a length of 5</para>
<para><code type='display'>func doStuff() { ... }</para>
<para>func foo(a, b, c int) {...}</para>
<para>func bar(d, e, f int) (x, y int, z float32) {...}</para>
<para></code></para>
<para>Function doStuff neither takes parameters nor has any return values/parameters.</para>
<para>The function foo takes the three parameters a,b and c all of type int.</para>
<para>The function bar takes the three parameters d, e and f all of type int but also returns x and y of type int and z of type float32.</para>
<para>We will have a closer look at all of that later. Now it is important to understand that you write declarations from left to right - the same way you read it.</para>
</topic>
<topic>
<title>Packages</title>
<topic>
<title>Introducing Packages</title>
<para>Let us first try to understand the structure of a Go source file. Let us create an empty file and name it as <b>GoTutorial.go</b>. When you will try to compile the above source file, you will get the following error:</para>
<para><code type='display'>package statement must be first</code></para>
<para>So, we come to know that package holds an important place in  Go Programming Language</para>
<para>Now open the file GoTutorial.go and add following line:-</para>
<para><code type='display'>package main </code></para>
<para>Compile the code. What do you observe? Yes, we got following error:-</para>
<para><code type='display'>runtime.mainstart: undefined: main.main</code></para>
<para>Hope you got the above error. If yes, then read ahead. If not, then check if your source file has the above line or not. </para>
<para>A program has to start somewhere. Right? So, where does a Go Program start? Java entry-point is public static void main function and for C, it is main. Similarly, the entry-point for Go Programming Language is the main function.</para>
<para>So, now do one thing, edit the source file and make sure that it looks like this:-</para>
<para><code type='display'>   package main</para>
<para>   func main(){</para>
<para>   }</code></para>
<para>Now try to compile the code. Yes, it will compile. </para>
<para>But it is not good. It does nothing. Let us try to print something. Modify the code</para>
<para><code type='display'>   package main</para>
<para>   import "fmt"   </para>
<para>   func main(){</para>
<para>        fmt.Println("Hello World")</para>
<para>   }</para>
<para></code></para>
<para>Now try to run the above code. The output will be:</para>
<para><code type='display'>Hello World</code></para>
<para>We imported a package called fmt and from that package we used a function called Println().</para>
<para>The packages are of utmost importance in Go. Every program should have one source file with package main and function main() as this is the starting point of a Go program.</para>
<para>Also, you can import different packages using import statement. Import here is more like an import in Java and not like inclusions in C as it doesn't includes the source of the imported packages into the source file. </para>
</topic>
<topic>
<title>Packages & Workspaces</title>
</topic>
</topic>
<topic>
<title>Types</title>
<topic>
<title>Basic Types</title>
<para>Go knows the following basic types:</para>
<ul>
    <li>string</li>
    <li>bool - represents a set of the two predeclared constants <i>true</i> and <i>false</i></li>
    <li>int - signed integer, 32 or 64 bits size (implementation-specific)</li>
    <li>int8 - signed integers, 8 bits size (-128 - 127)</li>
    <li>int16 - signed integers, 16 bits size (-32768 to 32767)</li>
    <li>int32 - signed integers, 32 bits size (-2147483648 to 2147483647)</li>
    <li>int64 - signed integers, 64 bits size (-9223372036854775808 to 9223372036854775807)</li>
    <li>uint -  unsigned integer, 32 or 64 bits size (implementation-specific), same size as int</li>
    <li>uint8 - unsigned integers, 8 bits size (0 to 255)</li>
    <li>uint16 - unsigned integers, 16 bits size (0 to 65535)</li>
    <li>uint32 - unsigned integers, 32 bits size (0 to 4294967295)</li>
    <li>uint64 - unsigned integers, 64 bits size (0 to 18446744073709551615)</li>
    <li>uintptr - unsigned integer, size large enough to hold a pointer value</li>
    <li>float32 - IEEE-754 32-bit floating-point</li>
    <li>float64 -IEEE-754 64-bit floating-point</li>
    <li>complex64 - complex numbers with float32 real and imaginary parts</li>
    <li>complex128 - complex numbers with float64 real and imaginary parts</li>
    <li>byte - just an alias for uint8</li>
    <li>rune - just an alias for int32, representing unicode code point</li>
</ul>
<para>Except <b>byte </b>and <b>rune </b>all numeric types are distinct.</para>
<para>Different numeric types mixed in an expression or assignment musst be converted. </para>
<para>Even though <b>int</b> is just an alias for <b>int32</b> in a 32-Bit architecture and an alias for <b>int64</b> in a 64-Bit architecture it must be converted if mixed in an expression or assignment.</para>
<para><b>string</b> is immutable, it is not possible to change its content once created.</para>
<para>The elements of a <b>string</b> are of type <b>byte</b> and can be accessed with normal indexing operations. </para>
<para>Example:<code type='display'>exampleString[1]</code>It is invalid to use the address of that element:<code type='display'> &exampleString[1]   //INVALID</code> </para>
<para>You can use <b>len </b><b></b>to determin the length of a string: <code type='display'>len(exampleString)</code></para>
</topic>
<topic>
<title>Arrays</title>
<para>An array is a sequenz of a distinct number of elements and those elements must be of a single type.</para>
<para>The length of that array is part of its type and must be a constant expression, evaluating to a zero or positiv (>-1) integer value. </para>
<para>A single array is always one-dimensional but can be composed to build more-dimensional types.</para>
<para>Examples:</para>
<para><code type='display'>[23]int64 //array of 23 elements of type int64</para>
<para>[3]float32 { 1.4, 0.3, 7.9 }</para>
<para>[2][3]int32</para>
<para>[]int32 {5, 7} //same as [2]int32 { 5, 7}</para>
<para></code></para>
<para>You can use <b>len</b> to determin the lenght of an array:</para>
<para><code type='display'>a := []int32 { 1, 4, 9, 6}</para>
<para>length = len(a) //lenght == 4</code></para>
<para>Arrays are passed by value and arrays are not dynamic.</para>
<para>You work not that often with arrays but with slices (see next section) for slices being a reference on a hidden array</para>
<para>and therefore dynamic.</para>
</topic>
<topic>
<title>Slices</title>
<para>A slice is similar to an array, but it can grow when new elements are added. A slice always refers to an underlying array. What makes slices different from arrays is that a slice is a pointer to an array; slices are reference types, which means that if you assign one slice to another, both refer to the same underlying array. For instance, if a function takes a slice argument, changes it makes to the elements of the slice will be visible to the caller, analogous to passing a pointer to the underlying array. With:</para>
<para><code type='display'>sl := make([]int, 10)</code></para>
<para>you create a slice which can hold ten elements. Note that the underlying array isn’t specified. A slice is always coupled to an array that has a fixed size. For slices we define a capacity and a length. Figure below depicts the following Go code. First we create an array of m elements of the type <code type='display'>int: var array[m]int</code></para>
<para>Next, we create a slice from this array: <code type='display'>slice := array[0:n]</code></para>
<para>And now we have:</para>
<ul>
    <li>len(slice)== n</li>
    <li>cap(slice)== m </li>
    <li>len(array)== cap(array)== m </li>
</ul>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/18829454/array_vs_slice.png'/></div>
<para><br /></para>
<para>Given an array, or another slice, a new slice is created via a&#91;I:J&#93;. This creates a new slice which refers to the variable a, starts at index I, and ends before index J. It has length J - I.</para>
<para><i>// array&#91;n:m&#93;, create a slice from array with elements n to m-1</i></para>
<para><code type='display'>a := [...]int{1, 2, 3, 4, 5}</para>
<para>s1 := a[2:4]</para>
<para>s2 := a[1:5]</para>
<para>s3 := a[:]</para>
<para>s4 := a[:4]</para>
<para>s5 := s2[:]</para>
<para></code></para>
<ol>
    <li>Define an array with 5 elements, from index 0 to 4;</li>
    <li>Create a slice with the elements from index 2 to 3, this contains: 3, 4;</li>
    <li>Create a slice with the elements from index 1 to 4, contains: 2, 3, 4, 5;</li>
    <li>Create a slice with all the elements of the array in it. This is a shorthand for: a&#91;0:len(a)&#93;;</li>
    <li>Create a slice with the elements from index 0 to 3, this is thus short for: a&#91;0:4&#93;, and yields: 1, 2, 3, 4;</li>
    <li>Create a slice from the slice s2, note that s5 still refers to the array a.</li>
</ol>
<para>In the code listed next we dare to do the impossible on line <b><i><u>slice&#91;99&#93; = 'a'</u></i></b> and try to allocate something beyond the capacity (maximum length of the underlying array) and we are greeted with a runtime error.</para>
<para><i>Arrays and slices</i></para>
<para><code type='display'>package main </para>
<para>func main() { </para>
<para>var array [100]int <i>// Create array, index from 0 to 99</i> </para>
<para>slice := array[0:99] <i>// Create slice, index from 0 to 98 </i></para>
<para>slice[98] = 'a' <i>// OK </i></para>
<para>slice[99] = 'a' <i>// Error: "throw: index out of range" </i></para>
<para>}</code></para>
<para>If you want to extend a slice, there are a couple of built-in functions that make life easier:</para>
<para><b>append</b> and <b>copy</b>. </para>
<para><i>The function <b>append</b> appends zero or more values x to a slice s and returns the resulting slice, with the same type as s. If the capacity of s is not large enough to fit the additional values, append allocates a new, sufficiently large slice that fits both the existing slice elements and the additional values. Thus, the returned slice may refer to a different underlying array.</i></para>
<para><code type='display'>s0 := []int{0, 0}</para>
<para>s1 := append(s0, 2)</para>
<para>s2 := append(s1, 3, 5, 7)</para>
<para>s3 := append(s2, s0...)</para>
<para></code></para>
<ol start="2">
    <li>append a single element, s1 == &#91;&#93;int{0, 0, 2};</li>
    <li>append multiple elements, s2 == &#91;&#93;int{0, 0, 2, 3, 5, 7};</li>
    <li>append a slice, s3 == &#91;&#93;int{0, 0, 2, 3, 5, 7, 0, 0}. Note the three dots!</li>
</ol>
<para>And</para>
<para><i>The function copy copies slice elements from a source src to a destination dst and returns the number of elements copied. Source and destination may overlap. The number of elements copied is the minimum of len(src) and len(dst).</i></para>
<para><code type='display'>var a = [...]int{0, 1, 2, 3, 4, 5, 6, 7}</para>
<para>var s = make([]int, 6)</para>
<para>n1 := copy(s, a[0:])     <i>// n1 == 6, s == []int{0, 1, 2, 3, 4, 5}</i></para>
<para>n2 := copy(s, s[2:])     <i>// n2 == 4, s == []int{2, 3, 4, 5, 4, 5}</i></para>
<para></code></para>
<para><b>Re-slicing</b></para>
<para>The length of a slice may be changed as long as it still fits within the limits of the underlying array; just assign it to a slice of itself. The capacity of a slice, accessible by the built-in function cap, reports the maximum length the slice may assume. Here is a function to append data to a slice. If the data exceeds the capacity, the slice is reallocated. The resulting slice is returned. The function uses the fact that len and cap are legal when applied to the nil slice, and return 0.</para>
<para><code type='display'>func Append(slice, data[]byte) []byte {</para>
<para>    l := len(slice)</para>
<para>    if l + len(data) > cap(slice) {  // reallocate</para>
<para>        // Allocate double what's needed, for future growth.</para>
<para>        <b>newSlice := make([]byte, (l+len(data))*2)</b></para>
<para>        // The copy function is predeclared and works for any slice type.</para>
<para>        <b>copy(newSlice, slice)</b></para>
<para>        <b>slice = newSlice</b></para>
<para>    }</para>
<para>    slice = slice[0:l+len(data)]</para>
<para>    for i, c := range data {</para>
<para>        slice[l+i] = c</para>
<para>    }</para>
<para>    return slice</para>
<para>}</para>
<para></code></para>
<para>We must return the slice afterwards because, although Append can modify the elements of slice, the slice itself (the run-time data structure holding the pointer, length, and capacity) is passed by value.</para>
</topic>
<topic>
<title>Structs</title>
</topic>
<topic>
<title>Pointers & 'new'</title>
</topic>
<topic>
<title>Maps</title>
</topic>
</topic>
<topic>
<title>Variables & Constants</title>
</topic>
<topic>
<title>Operators</title>
</topic>
<topic>
<title>Formatting & Commentary</title>
<para>Go provides two tools for formatting and documentation.</para>
<para>In this section we have a look at those tools and how commentary is done in Go.</para>
<topic>
<title>Formatting</title>
<para>Formatting your code is always an issue.</para>
<para>Go provides the program <b>gofmt</b> (<b>go fmt</b> for package level). It reads your Go program and returns the source with a standard vertical alignment, indentation, retaining and reformatted comments if needed. </para>
<para>Run <b>gofmt</b> after your layout changed. </para>
<para>Don't ignore it, if the outcome is not what you expected. Rearrange your code so it fits.</para>
<para>Some other details regarding formatting are:</para>
<ul>
    <li><b>Parentheses</b>
Go needs less parentheses than other languages.
Control structures (if, for, switch) for example do not have parentheses in their syntax. </li>
    <li><b>Indentation</b>
    We use tabs for indentation, not spaces. Use spaces only if really needed. <b>gofmt</b> adds tabs if useful</li>
    <li><b>Line length</b>
    There is no line length limit in Go. But long lines might be hard to read, better wrap very long lines and indent it with an extra tab. 
</li>
</ul>
</topic>
<topic>
<title>Comments</title>
<para>In Go you can comment in two different ways.</para>
<para>A block comment is done with<b> /*</b> at the beginning of the comment, <b>*/</b> at the end and a line comment with <b>//</b> at the beginning of your line.</para>
<para>Normally you use line comments, block comments are most likely used for package comments or  for disabling code blocks you don't need at the moment.</para>
<para>Examples:</para>
<para><code type='display'>/* this is an example for a block code</para>
<para>for example to comment your package</para>
<para>*/</para>
<para>package comments</para>
<para>// and this is a line comment</para>
<para></code></para>
<para>It is possible to combine a block comment and a line comment but you must not put a block comment in another block comment.</para>
<para>Every package should be commented with a block comment preceding the package clause. Only simple packages should be briefly commented with line comments.</para>
<para>The <b>godoc</b> program extracts documentation from the Go source files. The comments that appear before top-level declarations, with no intervening newlines, serve together with the declarationas documentation for the item. The quality of the documentation, created by godoc, depends on the quality of those comments.</para>
<para>If your package consists of more than one file it is sufficient to put the package comment in one file - no matter which file, any one will do. The package comment should give a brief introduction of the package and provide all relevant information for the complete package. The godoc page will start with that package comment, therefore it should give all the information needed to understand the following, detailed documentation.</para>
<para><b>godoc </b>creates uninterpreted plain text, HTML or other annotations should not be used.</para>
<para>Do not use ******* or so for formatting and do not try to align the lines with spacing because the output generated by <b>godoc</b> may not be presented with a fixed-width font.</para>
<para>Write that comments the way everyone can read and understand it easily, use good sentence structure, correct spelling, punctation, split long sentences or fold them at least and so on.</para>
<para>Any exported (capitalized) name in a program should have a doc comment!</para>
</topic>
</topic>
</topic>
</topic>
