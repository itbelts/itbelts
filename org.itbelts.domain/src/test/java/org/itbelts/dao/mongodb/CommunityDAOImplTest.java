/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongodb;

import static org.junit.Assert.*;

import java.util.List;
import java.util.logging.Logger;

import org.itbelts.domain.Community;
import org.junit.Test;

/**
 *  Unit test for the CommunityDAOImpl class.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2014.6.0  02-jun-2014 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2014.6.0  02-jun-2014
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class CommunityDAOImplTest {

    private CommunityDAOImpl myUnitUnderTest = new CommunityDAOImpl();
    private static final Logger LOGGER = Logger.getLogger( CommunityDAOImplTest.class.getName() );

    /**
     * Test method for {@link org.itbelts.dao.mongodb.CommunityDAOImpl#getCommunities()}.
     */
    @Test
    public void testGetCommunities() {
        List<Community> theList = myUnitUnderTest.getCommunities();
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        LOGGER.info( theList.toString() );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongodb.CommunityDAOImpl#getCommunity(java.lang.String)}.
     */
    @Test
    public void testGetCommunity() {
        Community theJavaCommunity = myUnitUnderTest.getCommunity( "J" );
        assertNotNull( theJavaCommunity );
        assertEquals( "J", theJavaCommunity.get_id() );
        LOGGER.info( theJavaCommunity.toString() );
    }

}
