package org.itbelts.dao.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.logging.Logger;

import org.itbelts.domain.Question;
import org.itbelts.domain.QuestionCategory;
import org.junit.Test;

public class QuestionDAOImplTest {

    private QuestionDAOImpl myUnitUnderTest = new QuestionDAOImpl();
    private static final Logger LOGGER = Logger.getLogger( QuestionDAOImplTest.class.getName() );

    @Test
    public void testGetQuestionsCategoriesForExam() {
        List<QuestionCategory> theList = myUnitUnderTest.getQuestionsCategoriesForExam( "J0515-E1" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        assertEquals( "J0515-E1", theList.get(0).getExamId() );
        LOGGER.info( theList.toString() );
    }

    @Test
    public void testGetQuestionsForCategory() {
        List<Question> theList = myUnitUnderTest.getQuestionsForCategory( "J0515-E1-C01" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        assertEquals( "J0515-E1-C01", theList.get(0).getQuestionCategoryId() );
        LOGGER.info( theList.toString() );
    }

    @Test
    public void testGetQuestion() {
        Question theQuestion= myUnitUnderTest.getQuestion( "J0515-E1-Q0001" );
        assertNotNull( theQuestion );
        assertEquals( "Primary Key Attributes", theQuestion.getName() );
        LOGGER.info( theQuestion.toString() );
    }

    @Test
    public void testBuildExam() {
        List<Question> theList = myUnitUnderTest.buildExam( "J0515-E1" );
        assertNotNull( theList );
        assertEquals( 20, theList.size() );
        assertTrue( theList.get(0).getQuestionCategoryId().startsWith( "J0515-E1" ) );
        LOGGER.info( theList.toString() );
    }
}
