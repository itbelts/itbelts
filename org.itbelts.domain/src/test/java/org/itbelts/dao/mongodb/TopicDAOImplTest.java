/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.logging.Logger;

import org.itbelts.domain.Topic;
import org.junit.Test;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 22 Oct 2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      22 Oct 2013
 * @author <a href="mailto:koen.bruyndonckx@axa.be"> Koen Bruyndonckx </a>
 */
public class TopicDAOImplTest {

    private TopicDAOImpl myUnitUnderTest = new TopicDAOImpl();
    private static final Logger LOGGER = Logger.getLogger( TopicDAOImplTest.class.getName() );

    /**
     * Test method for {@link org.itbelts.dao.mongolab.TopicDAOImpl#getTopics()}.
     */
    @Test
    public void testGetTopics() {
        List<Topic> theList = myUnitUnderTest.getTopics();
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        LOGGER.info( theList.toString() );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.TopicDAOImpl#getTopicsForSpecialty(java.lang.String)}.
     */
    @Test
    public void testGetTopicsForSpecialty() {
        List<Topic> theList = myUnitUnderTest.getTopicsForSpecialty( "J01" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        for ( Topic t : theList ) {
            assertEquals( "J01", t.getSpecialtyId() );
        }
        LOGGER.info( theList.toString() );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.TopicDAOImpl#getTopicsForCommunity(java.lang.String)}.
     */
    @Test
    public void testGetTopicsForCommunity() {
        List<Topic> theList = myUnitUnderTest.getTopicsForCommunity( "J" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        for ( Topic t : theList ) {
            assertTrue( t.getSpecialtyId().startsWith( "J" ) );
        }
        LOGGER.info( theList.toString() );
    }

}
