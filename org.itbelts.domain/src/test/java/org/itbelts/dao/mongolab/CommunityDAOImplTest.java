/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongolab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.itbelts.dao.mongolab.CommunityDAOImpl;
import org.itbelts.domain.Community;
import org.junit.Test;

/**
 *  Integration test to see if the MongoDB database has communities in it.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 22 Oct 2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      22 Oct 2013
 * @author <a href="mailto:koen.bruyndonckx@axa.be"> Koen Bruyndonckx </a>
 */
public class CommunityDAOImplTest {

    private CommunityDAOImpl myUnitUnderTest = new CommunityDAOImpl();

    /**
     * Test method for {@link org.itbelts.dao.mongolab.CommunityDAOImpl#getCommunities()}.
     */
    @Test
    public void testGetCommunities() {
        List<Community> theList = myUnitUnderTest.getCommunities();
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        System.out.println( theList );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.CommunityDAOImpl#getCommunity(java.lang.String)}.
     */
    @Test
    public void testGetCommunity() {
        Community theJavaCommunity = myUnitUnderTest.getCommunity( "J" );
        assertNotNull( theJavaCommunity );
        assertEquals( "J", theJavaCommunity.get_id() );
        System.out.println( theJavaCommunity );
    }

}
