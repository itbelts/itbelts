/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongolab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.itbelts.dao.mongolab.TopicDAOImpl;
import org.itbelts.domain.Topic;
import org.junit.Test;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 22 Oct 2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      22 Oct 2013
 * @author <a href="mailto:koen.bruyndonckx@axa.be"> Koen Bruyndonckx </a>
 */
public class TopicDAOImplTest {

    private TopicDAOImpl myUnitUnderTest = new TopicDAOImpl();

    /**
     * Test method for {@link org.itbelts.dao.mongolab.TopicDAOImpl#getTopics()}.
     */
    @Test
    public void testGetTopics() {
        List<Topic> theList = myUnitUnderTest.getTopics();
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        System.out.println( theList );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.TopicDAOImpl#getTopicsForSpecialty(java.lang.String)}.
     */
    @Test
    public void testGetTopicsForSpecialty() {
        List<Topic> theList = myUnitUnderTest.getTopicsForSpecialty( "J01" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        for ( Topic t : theList ) {
            assertEquals( "J01", t.getSpecialtyId() );
        }
        System.out.println( theList );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.TopicDAOImpl#getTopicsForCommunity(java.lang.String)}.
     */
    @Test
    public void testGetTopicsForCommunity() {
        List<Topic> theList = myUnitUnderTest.getTopicsForCommunity( "J" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        for ( Topic t : theList ) {
            assertTrue( t.getSpecialtyId().startsWith( "J" ) );
        }
        System.out.println( theList );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.TopicDAOImpl#getTopic(java.lang.String)}.
     */
    @Test
    public void testGetTopic() {
        Topic theTopic = myUnitUnderTest.getTopic( "J0101" );
        assertNotNull( theTopic );
        assertTrue( theTopic.getSpecialtyId().startsWith( "J" ) );
        System.out.println( theTopic );
    }
    
}
