package org.itbelts.dao;

import java.util.List;

import org.itbelts.domain.UserExam;

/**
 * Interface that describes how to manage user exam information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 05-nov.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      05-nov.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface IUserExamDAO {

    /**
     * Get all exams a user has taken.
     * @param aUserId
     * @return List<UserExam>
     */
    public List<UserExam> getExamsForUser( String aUserId );

    /**
     * Get a specific user exam.
     * @param aUserExamId
     * @return {@link UserExam}
     */
    public UserExam getUserExam( String aUserExamId );
    
    /**
     * Store the given user exam.
     * 
     * @param aUserExam
     * @return {@link UserExam}
     */
    public UserExam saveUserExam( UserExam aUserExam );
}
