package org.itbelts.dao;

import java.util.List;

import org.itbelts.domain.Exam;

/**
 * Interface that describes how to manage exam information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 28-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      28-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface IExamDAO {

    /**
     * Get all exams from the DB.
     * @return List<Course>
     */
    public List<Exam> getExams();

    /**
     * Get all exams that belong to a specialty within the given community.
     * @param aCommunityId
     * @return List<Exam>
     */
    public List<Exam> getExamsForCommunity( String aCommunityId );

    /**
     * Get a specific exam.
     * @param anExamId
     * @return {@link Exam}
     */
    public Exam getExam( String anExamId );
    
    
}
