package org.itbelts.dao;

import java.util.List;

import org.itbelts.domain.Topic;

/**
 * Interface that describes how to manage topic information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface ITopicDAO {

    public List<Topic> getTopics();

    public List<Topic> getTopicsForSpecialty( String aSpecialtyId );

    public List<Topic> getTopicsForCommunity( String aCommunityId );

    public Topic getTopic( String aTopicId );
}
