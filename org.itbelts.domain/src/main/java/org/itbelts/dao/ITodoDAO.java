package org.itbelts.dao;

import java.util.List;

import org.itbelts.domain.Todo;


/**
 * Interface that describes how to manage todos.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 16-nov.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      16-nov.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface ITodoDAO {

    /**
     * Store the given todo.
     * 
     * @param aTodo
     * @return {@link Todo}
     */
    public Todo saveTodo( Todo aTodo );

    /**
     * Get the todos for a given topic ID.
     * 
     * @param aTopicId
     * @return List<Todo>
     */
    public List<Todo> getTodosForTopic( String aTopicId );

    /**
     * Get the todos for a given course ID.
     * 
     * @param aCourseId
     * @return List<Todo>
     */
    public List<Todo> getTodosForCourse( String aCourseId );
    
    /**
     * Get the todos for a given course exam.
     * 
     * @param anExamId
     * @return List<Todo>
     */
    public List<Todo> getTodosForExam( String anExamId );

    /**
     * Get a spcific todo.
     * 
     * @param aTodoId
     * @return Todo
     */
    public Todo getTodo( String aTodoId );
    
    /**
     * Get all todos.
     * 
     * @return List<Todo>
     */
    public List<Todo> getTodos();
}
