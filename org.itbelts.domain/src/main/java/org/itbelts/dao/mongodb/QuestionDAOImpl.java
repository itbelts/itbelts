package org.itbelts.dao.mongodb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.itbelts.dao.IQuestionDAO;
import org.itbelts.domain.AnswerContainer;
import org.itbelts.domain.Question;
import org.itbelts.domain.QuestionCategory;
import org.itbelts.domain.UserExam;

import com.mongodb.BasicDBObject;

/**
 * MongoDB implementation for the IQuestionDAO. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.12.0 01-nov.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.12.0 01-nov.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class QuestionDAOImpl extends AbstractMongoDbDAO implements IQuestionDAO {

    private static final String COLLECTION = "questions"; 
    private static final String CATEGORY_COLLECTION = "questioncategories"; 

    @Override
    public List<QuestionCategory> getQuestionsCategoriesForExam( String anExamId ) {
        return readFrom( CATEGORY_COLLECTION, QuestionCategory.class, new BasicDBObject("examId", anExamId ), null );
    }

    @Override
    public List<Question> getQuestionsForCategory( String aCategoryId ) {
        return readFrom( COLLECTION, Question.class, new BasicDBObject("questionCategoryId", aCategoryId ), null );
    }

    @Override
    public Map<String,Question> getQuestionsForUserExam( UserExam aUserExam ) {
        
        Map<String,Question> theMap = new HashMap<>();
        if ( aUserExam.getAnswers() == null || aUserExam.getAnswers().size() == 0 ) {
            return theMap;
        }
        
        try {
            for ( AnswerContainer ac : aUserExam.getAnswers() ) {
                theMap.put( ac.getQuestionId(), getQuestion( ac.getQuestionId() ) );
            }
        } catch ( Exception e ) {
            // This could fail if the userExam points to questions that are no longer available.  We'll deal with this in the view controller of the exam
        }
        return theMap;
    }
    
    @Override
    public Question getQuestion( String aQuestionId ) {
        return readFrom( COLLECTION, Question.class, new BasicDBObject("_id", aQuestionId ), null ).get( 0 );
    }

    @Override
    public List<Question> buildExam( String anExamId ) {
        
        List<Question> theExam = new ArrayList<Question>();
        
        // the list of questions needs to come from all question categories.  So we first read all categories for the exam.
        List<QuestionCategory> theCategories = getQuestionsCategoriesForExam( anExamId );
        
        // Then, for every category, according to its number of questions, we compose a random set
        Random r = new Random();
        for ( QuestionCategory c : theCategories ) {

            List<Question> theQuestions = getQuestionsForCategory( c.get_id() );
            
            int theAmount = c.getNumberOfQuestions();
            while ( theAmount > 0 ) {
                int theChosenQuestionIndex = r.nextInt( theQuestions.size() );
                theExam.add( theQuestions.get( theChosenQuestionIndex ) );
                theQuestions.remove( theChosenQuestionIndex );
                theAmount--;
            }
        }
        return theExam;
    }

}
