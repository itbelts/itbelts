package org.itbelts.dao.mongodb;

import java.util.List;

import org.itbelts.dao.ITodoDAO;
import org.itbelts.domain.Todo;

import com.mongodb.BasicDBObject;

/**
 * MongoDB implementation for the ITodoDAO. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.12.0 16-nov.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.12.0 16-nov.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class TodoDAOImpl extends AbstractMongoDbDAO implements ITodoDAO {

    private static final String COLLECTION = "todos";

    @Override
    public Todo saveTodo( Todo aTodo ) {
        sendTo( COLLECTION, aTodo );
        
        return aTodo;
    }

    @Override
    public List<Todo> getTodosForTopic( String aTopicId ) {
        return readFrom( COLLECTION, Todo.class, new BasicDBObject("topicId", aTopicId ), null );
    }

    @Override
    public List<Todo> getTodosForCourse( String aCourseId ) {
        return readFrom( COLLECTION, Todo.class, new BasicDBObject("courseId", aCourseId ), null );
    }
    
    @Override
    public List<Todo> getTodosForExam( String anExamId ) {
        return readFrom( COLLECTION, Todo.class, new BasicDBObject("examId", anExamId ), null );
    } 

    @Override
    public Todo getTodo( String aTodoId ) {
        return readFrom( COLLECTION, Todo.class, new BasicDBObject("_id", aTodoId ), null ).get(  0 );
    } 
    
    @Override
    public List<Todo> getTodos() {
        return readFrom( COLLECTION, Todo.class, null, null );
    } 


}
