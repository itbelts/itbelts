package org.itbelts.dao.mongodb;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.itbelts.exception.ITBeltsInternalException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;

/**
 * Base class for anything that needs to access our local MongoDB. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2014.6.0  02-jun-2014 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2014.6.0 02-jun-2014
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public abstract class AbstractMongoDbDAO {

    private static final Logger LOGGER = Logger.getLogger( AbstractMongoDbDAO.class.getName() );

    private static DB           itbelts;

    /**
     * Return a connection to the local itbelts MongoDB. You should make sure MONGOD is running and listening on port 8889
     * 
     * @return
     */
    public static DB getClient() {
        if ( itbelts == null ) {
            try {
                Mongo theDB = new Mongo( "localhost", 8889 );
                itbelts = theDB.getDB( "itbelts" );
            } catch ( UnknownHostException e ) {
                throw new ITBeltsInternalException( "Problem connecting to the itbelts database at port 8889.  You should make sure mongod is running.", e );
            }
        }

        return itbelts;
    }

    /**
     * Basic list retrieval.
     * 
     * @param aCollection
     *            The name of the collection in which to search
     * @param aType
     *            The Type of the every element in the list
     * @param aQuery
     *            optional query to limit the list
     * @return List<T> The list of the given type
     */
    protected <T> List<T> readFrom( String aCollection, Class<T> aType, DBObject aQuery, DBObject aFieldList ) {
        try {
            LOGGER.info( "Reading from " + aCollection );

            List<T> theList = new ArrayList<T>();

            if ( aQuery == null ) {
                aQuery = new BasicDBObject();
            }
            if ( aFieldList == null ) {
                aFieldList = new BasicDBObject();
            }
            LOGGER.info( "Using query " + aQuery + " and field list " + aFieldList );
            DBCursor cursor = getClient().getCollection( aCollection ).find( aQuery, aFieldList );
            try {
                Gson g = new Gson();
                while ( cursor.hasNext() ) {
                    theList.add( g.fromJson( cursor.next().toString(), aType ) );
                }
            } finally {
                cursor.close();
            }
            return theList;
        } catch ( Exception e ) {
            throw new ITBeltsInternalException( "Problem reading from collection " + aCollection, e );
        }

    }

    /**
     * Store an object into the DB.
     * 
     * @param aCollection
     * @param anObject
     */
    protected void sendTo( String aCollection, Object anObject ) {
        LOGGER.info( "Posting to " + aCollection + " --> object " + anObject );
        
        String json = new GsonBuilder().disableHtmlEscaping().create().toJson( anObject );
        LOGGER.info( json );
        
        getClient().getCollection( aCollection ).insert( (DBObject) JSON.parse( json ) );
    }

}
