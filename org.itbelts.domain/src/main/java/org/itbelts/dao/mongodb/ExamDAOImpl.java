package org.itbelts.dao.mongodb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.itbelts.dao.IExamDAO;
import org.itbelts.dao.ITopicDAO;
import org.itbelts.domain.Exam;
import org.itbelts.domain.Topic;

import com.mongodb.BasicDBObject;

/**
 * MongoDB implementation for the IExamDAO. <br>
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2014.6.0  02-jun-2014 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2014.6.0  02-jun-2014
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ExamDAOImpl extends AbstractMongoDbDAO implements IExamDAO {

    private ITopicDAO     myTopicDAO     = new TopicDAOImpl();
    private static final String COLLECTION = "exams"; 

    @Override
    public List<Exam> getExams() {
        return readFrom( COLLECTION, Exam.class, null, new BasicDBObject("_id", 1 ).append( "topicId", 1 ).append( "name", 1 ) );
    }

    @Override
    public List<Exam> getExamsForCommunity( String aCommunityId ) {
        
        // First get a complete list of all Exams  (we will filter out the ones we do not need later)
        List<Exam> theFullList = getExams();
        
        // Build a small Map of topic-ids for all topics belonging to the given community
        Map<String,Boolean> theTopicMap = new HashMap<String,Boolean>();
        
        List<Topic> theTopics = myTopicDAO.getTopicsForCommunity( aCommunityId );
        
        for ( Topic t : theTopics ) {
            theTopicMap.put( t.get_id(), Boolean.TRUE );
        }
        
        // Then remove all Exams that have a topic id that is not in the Map
        for ( int i = theFullList.size() -1 ; i >= 0 ; i-- ) {
            String theTopicId = theFullList.get( i ).getTopicId();
            if ( theTopicMap.get( theTopicId ) == null ) {
                theFullList.remove( i );
            }
        }
        
        return theFullList;
    }
    
    @Override
    public Exam getExam( String aExamId ) {
        return readFrom( COLLECTION, Exam.class, new BasicDBObject("_id", aExamId ), null ).get( 0 );
    }

}
