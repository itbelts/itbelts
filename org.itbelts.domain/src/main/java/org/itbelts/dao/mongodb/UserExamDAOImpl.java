package org.itbelts.dao.mongodb;

import java.util.List;

import org.itbelts.dao.IUserExamDAO;
import org.itbelts.domain.UserExam;

import com.mongodb.BasicDBObject;

/**
 * MongoDB implementation for the IUserExamDAO. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.12.0 05-nov.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.12.0 05-nov.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class UserExamDAOImpl extends AbstractMongoDbDAO implements IUserExamDAO {

    private static final String COLLECTION = "userexams"; 

    @Override
    public List<UserExam> getExamsForUser( String aUserId ) {
        return readFrom( COLLECTION, UserExam.class, new BasicDBObject("userId", aUserId ), null );
    }
    
    @Override
    public UserExam getUserExam( String aUserExamId ) {
        return readFrom( COLLECTION, UserExam.class, new BasicDBObject("_id", aUserExamId ), null ).get( 0 );
    }

    @Override
    public UserExam saveUserExam( UserExam aUserExam ) {
        sendTo( COLLECTION, aUserExam );
        
        return aUserExam;
    }

}
