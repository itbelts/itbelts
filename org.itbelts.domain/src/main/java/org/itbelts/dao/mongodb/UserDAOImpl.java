package org.itbelts.dao.mongodb;

import java.util.List;

import org.itbelts.dao.IUserDAO;
import org.itbelts.domain.User;

import com.mongodb.BasicDBObject;

/**
 *  MongoDB implementation for the IUserDAO.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class UserDAOImpl extends AbstractMongoDbDAO implements IUserDAO {

    private static final String COLLECTION = "users";

    @Override
    public User getUser( String anId ) {
        List<User> theList = readFrom( COLLECTION, User.class, new BasicDBObject("_id", anId ), null );
        
        if ( theList.size() == 0 ) {
            return null;
        }
        
        return theList.get( 0 );
    }

    @Override
    public User saveUser( User aUser ) {
        sendTo( COLLECTION, aUser );
        
        return aUser;
    }

}
