package org.itbelts.dao.mongodb;

import java.util.List;

import org.itbelts.dao.ICommunityDAO;
import org.itbelts.domain.Community;

import com.mongodb.BasicDBObject;

/**
 *  MongoDB implementation for the ICommunityDAO.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2014.6.0  02-jun-2014 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2014.6.0  02-jun-2014
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class CommunityDAOImpl extends AbstractMongoDbDAO implements ICommunityDAO {

    private static final String COLLECTION = "communities";
    
    @Override
    public List<Community> getCommunities() {
        return readFrom( COLLECTION, Community.class, null, null );
    }

    @Override
    public Community getCommunity( String aCommunityId ) {
        return readFrom( COLLECTION, Community.class, new BasicDBObject("_id", aCommunityId ), null ).get( 0 );
    }

}
