package org.itbelts.dao;

import java.util.List;

import org.itbelts.domain.Course;

/**
 * Interface that describes how to manage course information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface ICourseDAO {

    /**
     * Get all courses from the DB.
     * @return List<Course>
     */
    public List<Course> getCourses();

    /**
     * Get all courses that belong to a specialty within the given community.
     * @param aCommunityId
     * @return List<Course>
     */
    public List<Course> getCoursesForCommunity( String aCommunityId );

    /**
     * Get a specific courses.
     * @param aCommunityId
     * @return {@link Course}
     */
    public Course getCourse( String aCourseId );
    
    
}
