package org.itbelts.dao;

import org.itbelts.domain.User;

/**
 *  Interface that describes the ITBelts User info manipulation operations.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * 12 Oct 2013 - initial release
 *
 * </pre>
 *
 * @version 12 Oct 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface IUserDAO {

    /**
     * Retrieve the user with the given ID from the DB.
     * 
     * @param id     The user id  (result from logging on)
     * @return {@link User}  The user from the DB
     */
    public User getUser( String id );
    
    /**
     * Update the user information for the given user in the DB or create a new user if it did not exist.
     * 
     * @param user     The user instance with the new information
     * @return {@link User}  The updated user object OR null when update failed for some reason
     */
    public User saveUser( User user );
    
}
