package org.itbelts.dao.mongolab;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

import org.itbelts.dao.IUserDAO;
import org.itbelts.domain.User;

import com.google.gson.reflect.TypeToken;

/**
 *  MongoDB implementation for the IUserDAO.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class UserDAOImpl extends AbstractMongoLabDAO implements IUserDAO {

    private static final String PATH = "collections/users";

    @Override
    public User getUser( String anId ) {
        URL url = buildUrl( PATH, "q={\"_id\":\"" + anId + "\"}" );
        Type theListType = new TypeToken<List<User>>(){}.getType();
        
        List<User> theList = readFrom( url, theListType );
        
        if ( theList.size() == 0 ) {
            return null;
        }
        
        return theList.get( 0 );
    }

    @Override
    public User saveUser( User aUser ) {
        sendTo( buildUrl( PATH ), aUser );
        
        return aUser;
    }

//    @Override
//    public User deleteUser( User aUser ) {
//        deleteFrom( buildUrl( PATH, "\"_id\":\"" + aUser.get_id() + "\"" ) );
//        return aUser;
//    }

    
}
