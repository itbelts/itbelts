package org.itbelts.dao.mongolab;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

import org.itbelts.dao.ITodoDAO;
import org.itbelts.domain.Todo;

import com.google.gson.reflect.TypeToken;

/**
 * MongoDB implementation for the ITodoDAO. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.12.0 16-nov.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.12.0 16-nov.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class TodoDAOImpl extends AbstractMongoLabDAO implements ITodoDAO {

    private static final String PATH = "collections/todos";

    @Override
    public Todo saveTodo( Todo aTodo ) {
        sendTo( buildUrl( PATH ), aTodo );
        
        return aTodo;
    }

    @Override
    public List<Todo> getTodosForTopic( String aTopicId ) {
        URL url = buildUrl( PATH, "q={\"topicId\":\"" + aTopicId + "\"}" );
        Type theListType = new TypeToken<List<Todo>>(){}.getType();
        
        return readFrom( url, theListType );
    }

    @Override
    public List<Todo> getTodosForCourse( String aCourseId ) {
        URL url = buildUrl( PATH, "q={\"courseId\":\"" + aCourseId + "\"}" );
        Type theListType = new TypeToken<List<Todo>>(){}.getType();
        
        return readFrom( url, theListType );
    }
    
    @Override
    public List<Todo> getTodosForExam( String anExamId ) {
        URL url = buildUrl( PATH, "q={\"examId\":\"" + anExamId + "\"}" );
        Type theListType = new TypeToken<List<Todo>>(){}.getType();
        
        return readFrom( url, theListType );
    } 

    @Override
    public Todo getTodo( String aTodoId ) {
        URL url = buildUrl( PATH, "q={\"_id\":\"" + aTodoId + "\"}" );
        Type theListType = new TypeToken<List<Todo>>(){}.getType();
        
        List<Todo> theList = readFrom( url, theListType );
        
        if ( theList.size() == 0 ) {
            return null;
        }

        return theList.get( 0 );
    } 
    
    @Override
    public List<Todo> getTodos() {
        URL url = buildUrl( PATH );
        Type theListType = new TypeToken<List<Todo>>(){}.getType();
        
        return readFrom( url, theListType );
    } 
    
}
