package org.itbelts.domain;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * User in the site. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class User extends AbstractType {

    private static final long serialVersionUID = -2282390812654158614L;

    private String     _id;        // ID from Google
    private String     email;      // email from Google
    private String     name;       // name provided by the user in his/her profile page
    private String     countryCd;  // country selected by the user in his/her profile page
    
    // Maintained by the system
    private int        contributionPoints; 
    private int        rating;     // Value from 0 to 100
    private int        coachings;  // Number of ended coachings
    private List<BeltInfo> belts = new ArrayList<BeltInfo>();

    // Security related
    private List<RoleType> roles = new ArrayList<RoleType>();


    /**
     * Check to see if this user has a specific role.
     * @param aRoleType
     * @return boolean
     */
    public boolean hasRole( RoleType aRoleType ) {
        return (roles != null) && roles.contains( aRoleType );
    }
    
    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        if ( StringUtils.isBlank( name ) ) {
            return "User_" + get_id();
        }
        return name;
    }

    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }

    /**
     * @return Returns the countryCd.
     */
    public String getCountryCd() {
        return countryCd;
    }

    /**
     * @param aCountryCd The countryCd to set.
     */
    public void setCountryCd( String aCountryCd ) {
        countryCd = aCountryCd;
    }

    /**
     * @return Returns the contributionPoints.
     */
    public int getContributionPoints() {
        return contributionPoints;
    }

    /**
     * @param aContributionPoints The contributionPoints to set.
     */
    public void setContributionPoints( int aContributionPoints ) {
        contributionPoints = aContributionPoints;
    }

    /**
     * @return Returns the belts.
     */
    public List<BeltInfo> getBelts() {
        return belts;
    }

    /**
     * @param aBelts The belts to set.
     */
    public void setBelts( List<BeltInfo> aBelts ) {
        belts = aBelts;
    }

    /**
     * @return Returns the rating.
     */
    public int getRating() {
        return rating;
    }

    /**
     * @param aRating The rating to set.
     */
    public void setRating( int aRating ) {
        rating = aRating;
    }

    /**
     * @return Returns the coachings.
     */
    public int getCoachings() {
        return coachings;
    }

    /**
     * @param aCoachings The coachings to set.
     */
    public void setCoachings( int aCoachings ) {
        coachings = aCoachings;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param aEmail the email to set
     */
    public void setEmail(String aEmail) {
        email = aEmail;
    }

    /**
     * @return the roles
     */
    public List<RoleType> getRoles() {
        return roles;
    }

    /**
     * @param aRoles the roles to set
     */
    public void setRoles(List<RoleType> aRoles) {
        roles = aRoles;
    }


}
