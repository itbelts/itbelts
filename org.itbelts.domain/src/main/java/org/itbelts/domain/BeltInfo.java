package org.itbelts.domain;


/**
 * Container for belt information.
 * 
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class BeltInfo extends AbstractType {

    private static final long serialVersionUID = -8458767445452665726L;

    private Belt     level;
    private String   community;
    
    /**
     * @return Returns the level.
     */
    public Belt getLevel() {
        return level;
    }
    /**
     * @param aLevel The level to set.
     */
    public void setLevel( Belt aLevel ) {
        level = aLevel;
    }
    /**
     * @return Returns the community.
     */
    public String getCommunity() {
        return community;
    }
    /**
     * @param aCommunity The community to set.
     */
    public void setCommunity( String aCommunity ) {
        community = aCommunity;
    }
    
}
