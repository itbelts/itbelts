package org.itbelts.domain;

import java.util.UUID;

/**
 *  Defines a todo for a topic.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 16-nov.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      16-nov.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class Todo extends AbstractType {

    private static final long serialVersionUID = 6144008275959946226L;

    private String _id;
    private String topicId;
    private String courseId;
    private String examId;
    private String questionId;
    private String details;
    private String assignedTo;
    
    /**
     * Constructor.
     */
    public Todo() {
        _id = UUID.randomUUID().toString();
    }

    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }

    /**
     * @return Returns the topicId.
     */
    public String getTopicId() {
        return topicId;
    }

    /**
     * @param aTopicId The topicId to set.
     */
    public void setTopicId( String aTopicId ) {
        topicId = aTopicId;
    }

    /**
     * @return Returns the courseId.
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * @param aCourseId The courseId to set.
     */
    public void setCourseId( String aCourseId ) {
        courseId = aCourseId;
    }

    /**
     * @return Returns the examId.
     */
    public String getExamId() {
        return examId;
    }

    /**
     * @param aExamId The examId to set.
     */
    public void setExamId( String aExamId ) {
        examId = aExamId;
    }

    /**
     * @return Returns the questionId.
     */
    public String getQuestionId() {
        return questionId;
    }

    /**
     * @param aQuestionId The questionId to set.
     */
    public void setQuestionId( String aQuestionId ) {
        questionId = aQuestionId;
    }

    /**
     * @return Returns the details.
     */
    public String getDetails() {
        return details;
    }

    /**
     * @param aDetails The details to set.
     */
    public void setDetails( String aDetails ) {
        details = aDetails;
    }

    /**
     * @return Returns the assignedTo.
     */
    public String getAssignedTo() {
        return assignedTo;
    }

    /**
     * @param aAssignedTo The assignedTo to set.
     */
    public void setAssignedTo( String aAssignedTo ) {
        assignedTo = aAssignedTo;
    }

}
