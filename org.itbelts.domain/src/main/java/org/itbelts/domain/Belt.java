package org.itbelts.domain;

/**
 *  Belt type information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public enum Belt {

    White(0,1),
    Yellow(1,10),
    Orange(2,20),
    Green(3,30),
    Blue(4,40),
    Brown(5,50),
    Black(6,75);
    
    private final int level;
    private final int points;
    
    /**
     * Simple constructor to initialize the level of this belt.
     * 
     * @param aLevel
     * @param aPoints
     */
    private Belt( int aLevel, int aPoints ) {
        level = aLevel;
        points = aPoints;
    }

    
    /**
     * Can be used as an index in arrays.
     * 
     * @return Returns the level for this belt.
     */
    public int getLevel() {
        return level;
    }
    
    /**
     * @return Returns the points.
     */
    public int getPoints() {
        return points;
    }


    /**
     * Return the belt for the given level
     * @param aLevel   numeric value (stored in the DB)
     * @return {@link Belt}
     */
    public Belt forLevel( int aLevel ) {
        if ( aLevel == 6 ) return Black;
        if ( aLevel == 5 ) return Brown;
        if ( aLevel == 4 ) return Blue;
        if ( aLevel == 3 ) return Green;
        if ( aLevel == 2 ) return Orange;
        if ( aLevel == 1 ) return Yellow;
        
        return White;
    }
}
