package org.itbelts.domain;

/**
 *  Defines the communities (currently 4) in the system.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 19-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      19-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class Community extends AbstractType {

    private static final long serialVersionUID = 8611044374590552493L;

    private String _id;
    private String name;
    
    /**
     * Constructor.
     */
    public Community() {
    }

    /**
     * Constructor.
     * @param anId
     * @param aName
     */
    public Community( String anId, String aName ) {
        _id   = anId;
        name = aName;
    }

    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }
    
    
}
