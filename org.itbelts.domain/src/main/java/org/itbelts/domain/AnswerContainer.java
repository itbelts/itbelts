package org.itbelts.domain;

import java.util.ArrayList;
import java.util.List;


/**
 * Class that holds an answer for an exam question.<br>
 * <ul>
 * <li>Text answer: type="text" text=empty string
 * <li>Checkbox answer: type="checkbox" text=text from PossibleAnswer
 * <li>Radiobutton answer: type="radio" text=text from PossibleAnswer
 * </ul>
 * <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.12.0 27-okt.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.12.0 27-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class AnswerContainer extends AbstractType {

    private static final long    serialVersionUID = -77422075152775992L;

    /**
     * To match the original question to when calculating the exam result
     */
    private String               questionId;
    /**
     * Actual answers the user has given.<br>
     * <table>
     * <tr><th align="left">Type</th><th align="left">Not answered</th><th align="left">Answered</th></tr>
     * <tr><td>Text</td><td>[null]</td><td>["the answer"]</td></tr>
     * <tr><td>RadioButton (selected second answer)</td><td>[null, null, null, null, ...]</td><td>[null, "1", null, null, ...]</td></tr>
     * <tr><td>CheckBox (selected second and third answer)</td><td>[null, null, null, null, ...]</td><td>[null, "1", "2", null, ...]</td></tr>
     * </table>
     */
    private List<String>         givenAnswers     = new ArrayList<String>();         // Only has 1 element in case of text
    /**
     * boolean that indicates if the question has been answered correctly
     */
    private boolean              isCorrect        = false;
    /**
     * boolean that matches a given answer.  If it is correct, the boolean is true
     */
    private List<Boolean>        results          = new ArrayList<Boolean>();
    /**
     * true if the user wants to report a layout problem in this question
     */
    private boolean              reportLayoutProblem = false;

    
    /**
     * Default constructor used by Spring MVC.
     */
    public AnswerContainer() {
    }

    /**
     * Constructor that fills the question id.
     * @param aQuestionId
     */
    public AnswerContainer( String aQuestionId ) {
        questionId = aQuestionId;
    }
    
    /**
     * @return Returns the questionId.
     */
    public String getQuestionId() {
        return questionId;
    }

    /**
     * @param aQuestionId The questionId to set.
     */
    public void setQuestionId( String aQuestionId ) {
        questionId = aQuestionId;
    }

    /**
     * @return Returns the givenAnswers.
     */
    public List<String> getGivenAnswers() {
        return givenAnswers;
    }

    /**
     * @param aGivenAnswers
     *            The givenAnswers to set.
     */
    public void setGivenAnswers( List<String> aGivenAnswers ) {
        givenAnswers = aGivenAnswers;
    }

    /**
     * @return Returns the isCorrect.
     */
    public boolean isCorrect() {
        return isCorrect;
    }

    /**
     * @param aIsCorrect The isCorrect to set.
     */
    public void setCorrect( boolean aIsCorrect ) {
        isCorrect = aIsCorrect;
    }

    /**
     * @return Returns the results.
     */
    public List<Boolean> getResults() {
        return results;
    }

    /**
     * @param aResults The results to set.
     */
    public void setResults( List<Boolean> aResults ) {
        results = aResults;
    }

    /**
     * @return Returns the reportLayoutProblem.
     */
    public boolean isReportLayoutProblem() {
        return reportLayoutProblem;
    }

    /**
     * @param aReportLayoutProblem
     *            The reportLayoutProblem to set.
     */
    public void setReportLayoutProblem( boolean aReportLayoutProblem ) {
        reportLayoutProblem = aReportLayoutProblem;
    }

}
