package org.itbelts.domain;

/**
 *  Defines a specialty within a community.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 19-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      19-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class Specialty extends AbstractType {

    private static final long serialVersionUID = 7561108726518400719L;

    private String communityId;
    private String _id;
    private String name;
    
    /**
     * Constructor.
     */
    public Specialty() {
    }

    /**
     * Constructor.
     * @param aCommunityId
     * @param anId
     * @param aName
     */
    public Specialty( String aCommunityId, String anId, String aName ) {
        communityId = aCommunityId;
        _id   = anId;
        name = aName;
    }

    /**
     * @return Returns the communityId.
     */
    public String getCommunityId() {
        return communityId;
    }

    /**
     * @param aCommunityId The communityId to set.
     */
    public void setCommunityId( String aCommunityId ) {
        communityId = aCommunityId;
    }

    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }
    
    
}
