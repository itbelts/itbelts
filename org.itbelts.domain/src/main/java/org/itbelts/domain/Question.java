package org.itbelts.domain;

import java.util.ArrayList;
import java.util.List;


/**
 * Exam question. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 01 Nov 2013 - initial release
 *
 * </pre>
 *
 * @version 01 Nov 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class Question extends AbstractType {

    private static final long serialVersionUID = 3891514857967013826L;

    private String         _id;
    private String         name;
    private String         description;
    private String         explanation;
    private QuestionStatus status;
    private String         questionCategoryId;
    
    private List<PossibleAnswer> possibleAnswers = new ArrayList<PossibleAnswer>();
    
    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }
    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param aDescription The description to set.
     */
    public void setDescription( String aDescription ) {
        description = aDescription;
    }
    /**
     * @return Returns the explanation.
     */
    public String getExplanation() {
        return explanation;
    }
    /**
     * @param aExplanation The explanation to set.
     */
    public void setExplanation( String aExplanation ) {
        explanation = aExplanation;
    }
    /**
     * @return Returns the status.
     */
    public QuestionStatus getStatus() {
        return status;
    }
    /**
     * @param aStatus The status to set.
     */
    public void setStatus( QuestionStatus aStatus ) {
        status = aStatus;
    }
    /**
     * @return Returns the questionCategoryId.
     */
    public String getQuestionCategoryId() {
        return questionCategoryId;
    }
    /**
     * @param aQuestionCategoryId The questionCategoryId to set.
     */
    public void setQuestionCategoryId( String aQuestionCategoryId ) {
        questionCategoryId = aQuestionCategoryId;
    }
    /**
     * @return Returns the possibleAnswers.
     */
    public List<PossibleAnswer> getPossibleAnswers() {
        return possibleAnswers;
    }
    /**
     * @param aPossibleAnswers The possibleAnswers to set.
     */
    public void setPossibleAnswers( List<PossibleAnswer> aPossibleAnswers ) {
        possibleAnswers = aPossibleAnswers;
    }
    
}
