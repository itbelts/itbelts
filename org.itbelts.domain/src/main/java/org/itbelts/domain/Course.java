package org.itbelts.domain;

/**
 *  Defines a course in the system.  A course always belongs to a Topic. 
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 19-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      19-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class Course extends AbstractType {

    private static final long serialVersionUID = 4584110141816760473L;

    /**
     * The identifier is called _id to let MongoDB reuse it.<br>
     * The id starts with a C for a course.<br>
     * The id starts with a W for a workshop.
     */
    private String _id;
    /**
     * Foreign key towards the topics collection.
     */
    private String topicId;
    /**
     * Descriptive name for this course  (typically the same as the topic name)
     */
    private String name;
    /**
     * Full description text block (would typically include html tags for basic formatting)
     */
    private String description;
    /**
     * Status of the content
     */
    private CourseStatus status;

    /**
     * Constructor.
     */
    public Course() {
    }

    /**
     * Constructor.
     * @param aTopicId
     * @param anId
     * @param aName
     * @param aDescription
     */
    public Course( String aTopicId, String anId, String aName, String aDescription ) {
        topicId = aTopicId;
        _id   = anId;
        name = aName;
        description = aDescription;
        status = CourseStatus.released;
    }

    /**
     * Return a nice description of the status of this content
     * @return String  (used in course.jsp)
     */
    public String getStatusDescription() {
        if ( status == CourseStatus.beta ) {
            return "still in BETA";
        }
        
        return "RELEASED";
    }
    
    
    
    
    
    /**
     * @return Returns the topicId.
     */
    public String getTopicId() {
        return topicId;
    }

    /**
     * @param aTopicId The topicId to set.
     */
    public void setTopicId( String aTopicId ) {
        topicId = aTopicId;
    }

    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param aDescription The description to set.
     */
    public void setDescription( String aDescription ) {
        description = aDescription;
    }

    /**
     * @return Returns the status.
     */
    public CourseStatus getStatus() {
        return status;
    }

    /**
     * @param aStatus The status to set.
     */
    public void setStatus( CourseStatus aStatus ) {
        status = aStatus;
    }
    
    
}
