package org.itbelts.domain.extract;

import java.util.ArrayList;
import java.util.List;

import org.itbelts.domain.AbstractType;
import org.itbelts.domain.ExamStatus;

/**
 * Exam in the site. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ExamFromImport extends AbstractType {

    private static final long serialVersionUID = -1009622994038664518L;

    private String     id;
    private String     objectives;
    private ExamStatus status;
    private int        minutes;
    private int        percentage;
    private int        knowledgePoints;
    private int        contributionPoints;
    
    private List<ExamCategory> categories = new ArrayList<ExamCategory>();
    private List<ExamContributorFromImport> contributors = new ArrayList<ExamContributorFromImport>();

    /**
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }

    /**
     * @param aId The id to set.
     */
    public void setId( String aId ) {
        id = aId;
    }

    /**
     * @return Returns the objectives.
     */
    public String getObjectives() {
        return objectives;
    }

    /**
     * @param aObjectives The objectives to set.
     */
    public void setObjectives( String aObjectives ) {
        objectives = aObjectives;
    }

    /**
     * @return Returns the status.
     */
    public ExamStatus getStatus() {
        return status;
    }

    /**
     * @param aStatus The status to set.
     */
    public void setStatus( ExamStatus aStatus ) {
        status = aStatus;
    }

    /**
     * @return Returns the minutes.
     */
    public int getMinutes() {
        return minutes;
    }

    /**
     * @param aMinutes The minutes to set.
     */
    public void setMinutes( int aMinutes ) {
        minutes = aMinutes;
    }

    /**
     * @return Returns the percentage.
     */
    public int getPercentage() {
        return percentage;
    }

    /**
     * @param aPercentage The percentage to set.
     */
    public void setPercentage( int aPercentage ) {
        percentage = aPercentage;
    }

    /**
     * @return Returns the knowledgePoints.
     */
    public int getKnowledgePoints() {
        return knowledgePoints;
    }

    /**
     * @param aKnowledgePoints The knowledgePoints to set.
     */
    public void setKnowledgePoints( int aKnowledgePoints ) {
        knowledgePoints = aKnowledgePoints;
    }

    /**
     * @return Returns the contributionPoints.
     */
    public int getContributionPoints() {
        return contributionPoints;
    }

    /**
     * @param aContributionPoints The contributionPoints to set.
     */
    public void setContributionPoints( int aContributionPoints ) {
        contributionPoints = aContributionPoints;
    }

    /**
     * @return Returns the categories.
     */
    public List<ExamCategory> getCategories() {
        return categories;
    }

    /**
     * @param aCategories The categories to set.
     */
    public void setCategories( List<ExamCategory> aCategories ) {
        categories = aCategories;
    }

    /**
     * @return Returns the contributors.
     */
    public List<ExamContributorFromImport> getContributors() {
        return contributors;
    }

    /**
     * @param aContributors The contributors to set.
     */
    public void setContributors( List<ExamContributorFromImport> aContributors ) {
        contributors = aContributors;
    }

}
