package org.itbelts.domain.extract;

import java.util.ArrayList;
import java.util.List;

import org.itbelts.domain.AbstractType;
import org.itbelts.domain.PossibleAnswer;

/**
 * User in the site. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class QuestionFromImport extends AbstractType {

    private static final long serialVersionUID = 3891514857967013826L;

    private String     id;
    private String     title;
    private String     text;
    private String     explanation;
    private QuestionStatus     status;
    private String     category;
    
    private List<PossibleAnswer> answers = new ArrayList<PossibleAnswer>();

    /**
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }

    /**
     * @param aId The id to set.
     */
    public void setId( String aId ) {
        id = aId;
    }

    /**
     * @return Returns the title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param aTitle The title to set.
     */
    public void setTitle( String aTitle ) {
        title = aTitle;
    }

    /**
     * @return Returns the text.
     */
    public String getText() {
        return text;
    }

    /**
     * @param aText The text to set.
     */
    public void setText( String aText ) {
        text = aText;
    }

    /**
     * @return Returns the explanation.
     */
    public String getExplanation() {
        return explanation;
    }

    /**
     * @param aExplanation The explanation to set.
     */
    public void setExplanation( String aExplanation ) {
        explanation = aExplanation;
    }

    /**
     * @return Returns the answers.
     */
    public List<PossibleAnswer> getAnswers() {
        return answers;
    }

    /**
     * @param aAnswers The answers to set.
     */
    public void setAnswers( List<PossibleAnswer> aAnswers ) {
        answers = aAnswers;
    }

    /**
     * @return Returns the status.
     */
    public QuestionStatus getStatus() {
        return status;
    }

    /**
     * @param aStatus The status to set.
     */
    public void setStatus( QuestionStatus aStatus ) {
        status = aStatus;
    }

    /**
     * @return Returns the category.
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param aCategory The category to set.
     */
    public void setCategory( String aCategory ) {
        category = aCategory;
    }

}
