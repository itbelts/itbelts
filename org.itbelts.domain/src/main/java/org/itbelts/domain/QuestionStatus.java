package org.itbelts.domain;

/**
 *  Question status information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * 01 Nov 2013 - initial release
 *
 * </pre>
 *
 * @version 01 Nov 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public enum QuestionStatus {

    Beta,
    Released,
    Frozen;
}
