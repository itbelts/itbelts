package org.itbelts.domain;



/**
 * Container for 1 possible answer for a question.
 * 
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class PossibleAnswer extends AbstractType {

    private static final long serialVersionUID = -4311444127093187829L;

    private String      text;
    private AnswerType  type;
    private boolean     isCorrect;

    /**
     * @return Returns the text.
     */
    public String getText() {
        return text;
    }

    /**
     * @param aText The text to set.
     */
    public void setText( String aText ) {
        text = aText;
    }

    /**
     * @return Returns the type.
     */
    public AnswerType getType() {
        return type;
    }

    /**
     * @param aType The type to set.
     */
    public void setType( AnswerType aType ) {
        type = aType;
    }

    /**
     * @return Returns the isCorrect.
     */
    public boolean isCorrect() {
        return isCorrect;
    }

    /**
     * @param aIsCorrect The isCorrect to set.
     */
    public void setCorrect( boolean aIsCorrect ) {
        isCorrect = aIsCorrect;
    }

}
