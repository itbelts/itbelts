@cls
@SET OPATH=%PATH%
@SET M2_HOME=D:\data\kbr\dev\apache-maven-3.0.4
@SET JAVA_HOME=C:\J\JDK1.7.0
@SET PATH=%JAVA_HOME%\bin;%M2_HOME%\bin

@call mvn clean install -DskipTests=true
@copy target\org.itbelts.domain-1.0.0-SNAPSHOT.jar ..\AppEngine\ITBelts-webapp\war\WEB-INF\lib
@SET PATH=%OPATH%
pause
